---
date: 2022.09.12
title: "PyWeb-IL #99 - September 2022: PostgreSQL and Git internals"
register: https://www.meetup.com/pyweb-il/events/287549158/
group: PyWeb
number: 99
short_summary: "PostgreSQL and Git internals"
location:
  name: Swimm
  url: https://swimm.io
  address: "Daniel Frisch St 3, 6th floor, Tel Aviv-Yafo"
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/about-swim.md
  - presentation: presentations/cultural-learnings-of-postgresql.md
  - presentation: presentations/git-is-a-great-design.md
---
