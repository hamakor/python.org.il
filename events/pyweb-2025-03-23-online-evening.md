---
date: 2025.03.23
title: Creating a Chrome browser extension
register: https://www.meetup.com/pyweb-il/events/305328360/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/creating-a-chrome-browser-extension-virtual.md
    time: "20:30"
---
