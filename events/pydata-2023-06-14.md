---
date: 2023.06.14
title: PyData @ Explorium
group: PyData
register: https://www.meetup.com/pydata-tel-aviv/events/292573406/
location:
  name: Explorium
  url: https://www.explorium.ai/
  address: "Daniel Frisch St 3; Tel Aviv-Yafo"
schedule:
  - presentation: presentations/entity-resolution-at-explorium.md
  - presentation: presentations/the-hitchhikers-guide-to-labeled-data-quality.md
  - presentation: presentations/accelerating-data-excellence.md
---
