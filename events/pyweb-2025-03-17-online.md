---
date: 2025.03.17
title: Django settings with pydantic
register: https://www.meetup.com/pyweb-il/events/305494324/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/django-settings-with-pydantic-by-ruth-dubin-online.md
    time: "20:30"
---
