---
date: 2018.05.23
title: Mocking in Python as a testing tool - Workshop
register: https://www.meetup.com/PyWeb-IL/events/246802670/
group: PyWeb
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/mocking-in-python-by-gabor-szabo.md
    time: "18:00"
---
