---
date: 2024.05.19
title: "PyWeb-IL #106 - May 2024"
register: https://www.meetup.com/pyweb-il/events/300653333/
group: PyWeb
number: 106
location:
  name: ZenCity
  url: https://zencity.io/
  address: Carlebach St 20, Tel Aviv-Yafo
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/growing-up-a-data-story-by-alon-nisser.md
  - presentation: presentations/a-new-way-of-testing-web-frameworks.md
  - presentation: presentations/python-quiz-by-miki-tebeka.md
---
