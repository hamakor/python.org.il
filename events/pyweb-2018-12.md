---
date: 2018.12.10
title: "PyWeb-IL #78 - December 2018"
register: https://www.meetup.com/PyWeb-IL/events/255960438/
group: PyWeb
number: 78
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "HaUmanim 12, Tel-Aviv"
schedule:
  - presentation: presentations/i-hate-security-by-lior-mizrahi.md
    time: "18:00"
  - presentation: presentations/advanced-linux-cli-by-benny-daon.md
---
