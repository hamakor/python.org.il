---
date: 2018.01.16
title: Python Pair Programming with TDD Workshop
register: https://www.meetup.com/PyWeb-IL/events/246150120/
group: PyWeb
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/python-pair-programming-with-tdd-workshop.md
    time: "18:00"
---

