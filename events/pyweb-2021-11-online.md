---
date: 2021.11.01
title: "PyWeb-IL #94 - November 2021: Method chaining and Kubernetes"
register: https://www.meetup.com/PyWeb-IL/events/281228099/
group: PyWeb
number: 94
short_summary: "Method chaining and Kubernetes"
location:
  name: Online
schedule:
  - presentation: presentations/method-chaining-in-python-by-tal-einat.md
    time: "18:00"
  - presentation: presentations/kubernetes-cpu-requests-limits-and-noisy-neighbors.md
---
