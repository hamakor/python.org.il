---
date: 2019.05.28
title: Fixtures and Test Doubles in Pytest - Workshop
register: https://www.meetup.com/Code-Mavens/events/260824123/
group: PyMaven
location:
  name: Twiggle
  url: https://python.org.il/
  address: Yigal Alon St 94 · Tel Aviv-Yafo
schedule:
  - presentation: presentations/fixtures-and-test-doubles-in-python.md
    time: "18:00"
---
