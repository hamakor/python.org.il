---
date: 2024.07.08
group: PyWeb
number: 107
title: "PyWeb-IL #107 - July 2024"
register: https://www.meetup.com/pyweb-il/events/301554668/
location:
  name: Datarails
  url: https://www.datarails.com/
  address: 152 Derech Menachem Begin, floor 10, Tel Aviv-Yafo
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/how-to-make-a-computation-20000-times-faster.md
  - presentation: break
  - presentation: presentations/copilot-workspace-by-idan-gazit.md
---
