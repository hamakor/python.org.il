---
date: 2016.09.05
title: "PyWeb-IL #56 - September 2016"
register: https://www.meetup.com/PyWeb-IL/events/233235621/
group: PyWeb
number: 56
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/speedy-match-and-speedy-composer.md
    time: "18:00"
  - presentation: presentations/programming-pearls-in-python.md
  - presentation: presentations/testing-web-applications-using-selenium-and-python.md
---
