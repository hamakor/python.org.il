---
date: 2025.02.14
title: "Getting started with Git"
register: https://www.meetup.com/pyweb-il/events/305896213/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/getting-started-with-git.md
    time: "09:00"
---
