---
date: 2022.03.07
title: "PyWeb-IL #96 - March 2022: PostgreSQL and property-based testing"
register: https://www.meetup.com/PyWeb-IL/events/283152977/
group: PyWeb
number: 96
short_summary: "PostgreSQL and property-based testing"
location:
  name: Online
schedule:
  - presentation: presentations/property-based-testing-by-shai-geva.md
    time: "18:00"
  - presentation: presentations/using-reinforcement-learning-to-understand-our-society.md
---
