---
date: 2024.11.27
title: "PyWeb-IL #110 - November 2024"
register: https://www.meetup.com/pyweb-il/events/303504257/
group: PyWeb
number: 110
location:
  name: Infinidat
  url: https://www.infinidat.com/
  address: Akerstein Tower A 9 Hamenofim Street, Herzeliya Pituach.
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/welcome-to-infinidat.md
  - presentation: presentations/advanced-python-oo-by-miki-tebeka.md
  - presentation: presentations/boto-model.md
  - presentation: presentations/repl-driven-development-by-maor-kadosh.md
  - presentation: presentations/python-profiling-made-easy-by-christina-beletskaya.md


