---
date: 2020.11.02
title: "PyWeb-IL #88 - November 2020"
register: https://www.meetup.com/PyWeb-IL/events/273426130/
group: PyWeb
number: 88
location:
  name: Online
schedule:
  - presentation: presentations/the-design-and-development-of-choices-in-django.md
    time: "18:00"
  - presentation: presentations/gridroyale-a-life-simulation-for-exploring-social-dynamics.md
  - presentation: presentations/embedding-and-calling-python-with-0-serialization-and-epsylon-memory.md
---
