---
date: 2021.07.05
title: "PyWeb-IL #92 - July 2021: Caching and Spark"
register: https://www.meetup.com/PyWeb-IL/events/278338957/)
group: PyWeb
number: 92
short_summary: "Caching and Spark"
location:
  name: Online
schedule:
  - presentation: presentations/using-spark-with-python-some-tips-and-patterns.md
    time: "18:00"
  - presentation: presentations/two-hard-things-in-computer-science.md
---

