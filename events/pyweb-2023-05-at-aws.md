---
date: 2023.05.01
title: "PyWeb-IL #103 - May 2023: Pandas and OpenEdX"
register: https://www.meetup.com/pyweb-il/events/293084077/
group: PyWeb
number: 103
short_summary: "Pandas and OpenEdX"
location:
  name: AWS
  url: https://aws.amazon.com/
  address: "'Jerusalem' room at AWS, 28th floor, Azrieli Sarona, Derech Menachem Begin 121"
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/faster-pandas-by-miki-tebeka.md
  - presentation: presentations/campusil-architecture-by-roi-shillo.md
---
