---
date: 2015.05.04
title: "PyWeb-IL #44 - May 2015"
register: https://www.meetup.com/PyWeb-IL/events/221821038/
group: PyWeb
number: 44
location:
  name: TAMI
  url: https://wiki.telavivmakers.org/
  address: "Shoken 19, floor level, Tel Aviv"
schedule:
  - presentation: presentations/whats-new-in-django.md
    time: "18:00"
  - presentation: presentations/coroutines-magic-by-noam-elfanbaum.md
  - presentation: presentations/lispisms-things-python-and-django-can-learn-from-list.md
---
