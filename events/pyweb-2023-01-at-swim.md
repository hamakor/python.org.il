---
date: 2023.01.02
title: "PyWeb-IL #101 - January 2023: Variable scope and HTML over the wire"
register: https://www.meetup.com/pyweb-il/events/289904704/
group: PyWeb
number: 101
short_summary: "Variable scope and HTML over the wire"
location:
  name: Swimm
  url: https://swimm.io
  address: "Daniel Frisch St 3, 6th floor, Tel Aviv-Yafo"
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/variable-scope-and-closure-by-miki-tebeka.md
  - presentation: presentations/html-over-the-wire-by-meir-kriheli.md
---

