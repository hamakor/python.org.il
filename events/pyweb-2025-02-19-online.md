---
date: 2025.02.19
title: "Cultural Learnings of PostgreSQL for Make Benefit Glorious Company Product"
register: https://www.meetup.com/pyweb-il/events/305279328/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/cultural-learnings-of-postgresql-virtual.md
    time: "20:30"
---
