---
date: 2021.01.04
title: "PyWeb-IL #89 - January 2021"
register: https://www.meetup.com/PyWeb-IL/events/274652045/
group: PyWeb
number: 89
location:
  name: Online
schedule:
  - presentation: presentations/a-day-has-only-24-hours.md
    time: "18:00"
  - presentation: presentations/using-cython-to-run-a-cpp-synth-with-a-kivy-frontend-on-android.md
---
