---
date: 2021.05.10
title: "PyWeb-IL #91 - May 2021: Rust and string formatting"
register: https://www.meetup.com/PyWeb-IL/events/276694179/
group: PyWeb
number: 91
short_summary: "Rust and string formatting"
location:
  name: Online
schedule:
  - presentation: presentations/trust-the-process-by-meir-kriheli.md
    time: "18:00"
  - presentation: presentations/so-you-think-yo-can-print.md
---
