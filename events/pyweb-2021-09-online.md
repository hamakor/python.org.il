---
date: 2021.09.13
title: "PyWeb-IL #93 - September 2021: PostgreSQL indices and epistemology"
register: https://www.meetup.com/PyWeb-IL/events/279673741/
group: PyWeb
number: 93
short_summary: "PostgreSQL indices and epistemology"
location:
  name: Online
schedule:
  - presentation: presentations/unlocking-the-full-potential-of-postgresql-indexes-in-django.md
    time: "18:00"
  - presentation: presentations/epistemology-reflections-on-what-is-truthful-representation-of-reality-in-software-world.md
---
