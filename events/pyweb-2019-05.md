---
date: 2019.05.06
title: "PyWeb-IL #81 - May 2019: DevPi, TypeScript, post-Guido"
register: https://www.meetup.com/PyWeb-IL/events/259558201/
group: PyWeb
number: 81
short_summary: "DevPi, TypeScript, post-Guido"
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "HaUmanim 12 · Tel-Aviv"
schedule:
  - presentation: presentations/devpi-your-very-own-cheeseshop.md
    time: "18:00"
  - presentation: presentations/intro-to-typescript.md
  - presentation: presentations/pysnooper-pudb-and-a-pep-idea.md
---
