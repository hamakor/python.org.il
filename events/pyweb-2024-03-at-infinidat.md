---
date: 2024.03.18
title: "PyWeb-IL #105 - March 2024: Dataclasses and hacking 'import'"
register: https://www.meetup.com/pyweb-il/events/299210166/
group: PyWeb
number: 105
short_summary: "Dataclasses and hacking 'import'"
banner: /img/infinidat-20240318.jpg
location:
  name: Infinidat
  url: https://www.infinidat.com/
  address: Akerstein Tower A 9 Hamenofim Street, Herzeliya Pituach.
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/introducing-infinidat-by-rick-pelleg.md
  - presentation: presentations/python-dataclasses-by-elad-silberring.md
  - presentation: presentations/war-stories-by-elad-silberring.md
  - presentation: presentations/hacking-import-by-liad-oz.md
---
