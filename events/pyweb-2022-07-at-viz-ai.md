---
date: 2022.07.04
title: "PyWeb-IL #98 - July 2022: Vulnerabilities and PostgreSQL"
register: https://www.meetup.com/PyWeb-IL/events/285942509/
group: PyWeb
number: 98
short_summary: "Vulnerabilities and PostgreSQL"
location:
  name: Viz.AI
  url: https://www.viz.ai/
  address: "Derech Menachem Begin 150, Tel Aviv-Jaffa"
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/about-viz-ai.md
  - presentation: presentations/how-i-found-and-fixed-a-vulnerability-in-python.md
  - presentation: presentations/python-m-by-miki-tebeka.md
---
