---
date: 2019.11.04
title: "PyWeb-IL #84 - November 2019: Environment Setup, CPython on GitHub, and music"
register: https://www.meetup.com/PyWeb-IL/events/265117036/
group: PyWeb
number: 84
short_summary: "Environment Setup, CPython on GitHub, and music"
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "HaUmanim 12 · Tel-Aviv"
schedule:
  - presentation: presentations/the-perfect-python-environment-setup.md
    time: "18:00"
  - presentation: presentations/cpython-moved-to-github-how-does-it-work.md
  - presentation: presentations/live-coding-a-music-synthesizer.md
---
