---
date: 2023.09.11
title: "PyWeb-IL #104 - September 2023: Dash and reading code"
register: https://www.meetup.com/pyweb-il/events/295608063
group: PyWeb
number: 104
short_summary: "Dash and reading code"
location:
  name: MobileEye
  url: https://www.mobileye.com/
  address: Mordekhai Anielewicz St 52, Tel Aviv-Yafo
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/read-code-every-day.md
  - presentation: presentations/rapid-development-of-big-data-analytics-apps-using-dash.md
---
