---
date: 2023.07.04
title: PyCon Israel 2023 - Conference
register: https://pycon.org.il/2023/
group: PyConIL
location:
    name: Wohl Center
    url: https://wohl-center.com/wohl-conventions-center/
    address: Bar-Ilan University
schedule:
  - presentation: presentations/a-bugs-life.md
---
