---
date: 2018.03.05
title: "PyWeb-IL #74 - March 2018"
register: https://www.meetup.com/PyWeb-IL/events/247578175/
group: PyWeb
number: 74
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/python-performance-profiling-by-noam-elfanbaum.md
    time: "18:00"
  - presentation: presentations/4-languages-you-should-learn.md
  - presentation: presentations/shaping-serverless-architecture-with-domain-driven-design-patterns.md
---
