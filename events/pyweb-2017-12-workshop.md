---
date: 2017.12.19
title: Python testing workshop
register: https://www.meetup.com/PyWeb-IL/events/245433267/
group: PyWeb
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/testing-with-pytest-by-gabor-szabo.md
    time: "18:00"
---
