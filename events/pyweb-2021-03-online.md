---
date: 2021.03.01
title: "PyWeb-IL #90 - March 2021: WebRTC and simulations"
register: https://www.meetup.com/PyWeb-IL/events/275907259/
group: PyWeb
number: 90
short_summary: "WebRTC and simulations"
location:
  name: Online
schedule:
  - presentation: presentations/simulations-for-the-mathematically0challenged.md
    time: "18:00"
  - presentation: presentations/webrtc-real-time-communication-for-the-web.md
---
