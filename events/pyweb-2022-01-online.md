---
date: 2022.01.03
title: "PyWeb-IL #95 - January 2022: async.io and Pytest"
register: https://www.meetup.com/PyWeb-IL/events/282467260/
group: PyWeb
number: 95
short_summary: "async.io and Pytest"
location:
  name: Online
schedule:
  - presentation: presentations/python-async-io-from-zero-to-hero.m-by-eyal-balla.md
    time: "18:00"
  - presentation: presentations/postgresql-in-theory-and-practice-by-amir-more.md
---
