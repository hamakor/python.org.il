---
date: 2015.07.06
title: "PyWeb-IL #46 - July 2015"
register: https://www.meetup.com/PyWeb-IL/events/223077234/
group: PyWeb
number: 46
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/wheels-the-new-distribution-format-for-python-packages.md
    time: "18:00"
  - presentation: presentations/advanced-topics-in-django-admin-custom-fonts-fields-inlines-grapelli-and-other-amusing-hacks.md
  - presentation: presentations/using-pandas-for-data-processing.md
---
