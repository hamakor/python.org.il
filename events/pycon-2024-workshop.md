---
date: 2024.09.17
title: PyCon Israel 2024 - Workshop
register: https://pycon.org.il/2024/
group: PyConIL
location:
    name: Cinema City
    url: https://www.cinema-city.co.il/location/1
    address: Gelilot
schedule:
  - presentation: presentations/introduction-to-playwright-with-python.md
  - presentation: presentations/unleashing-the-power-of-llms-and-gen-ai-with-python.md
  - presentation: presentations/getting-started-with-grpc.md
  - presentation: presentations/codyssey-a-playful-python-workshop.md
  - presentation: presentations/introduction-to-geospatial-data-and-geopandas.md
---
