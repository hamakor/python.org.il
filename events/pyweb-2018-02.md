---
date: 2018.02.05
title: "PyWeb-IL #73 - February 2018"
register: https://www.meetup.com/PyWeb-IL/events/246639354/
group: PyWeb
number: 73
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/django-custom-model-fields-by-example.md
    time: "18:00"
  - presentation: presentations/application-level-blue-green-deployments-with-django.md
  - presentation: presentations/f--king-spectre-how-does-it-work.md
---
