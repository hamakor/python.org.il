---
date: 2017.09.04
title: "PyWeb-IL #68 - September 2017"
register: https://www.meetup.com/PyWeb-IL/events/242431694
group: PyWeb
number: 68
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/hypothesis-a-new-paradigm-in-testing.md
    time: "18:00"
  - presentation: presentations/actually-generate-swagger-for-drf-orgsdapiisui.md
  - presentation: presentations/test-automation-with-robot-framework.md
---
