---
date: 2022.11.14
title: "PyWeb-IL #100 - November 2022: Chrome extensions and CDC"
register: https://www.meetup.com/pyweb-il/events/288598977/
group: PyWeb
number: 100
short_summary: "Chrome extensions and CDC"
location:
  name: Swimm
  url: https://swimm.io
  address: "Daniel Frisch St 3, 6th floor, Tel Aviv-Yafo"
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/about-swim.md
  - presentation: presentations/creating-a-chrome-browser-extension.md
  - presentation: presentations/change-data-capture-for-advanced-integration.md
---
