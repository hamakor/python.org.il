---
date: 2024.07.25
title: "Testing Python with Pytest: The magic of fixtures"
register: https://www.meetup.com/code-mavens/events/301810834/
group: PyMaven
location:
  name: Online
schedule:
  - presentation: presentations/testing-python-with-pytest-the-magic-of-fixtures.md
---
