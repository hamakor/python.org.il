---
date: 2025.03.23
title: The Hitchhiker's Guide to Labeled Data Quality
register: https://www.meetup.com/pyweb-il/events/305976756/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/the-hitchhikers-guide-to-labeled-data-quality-online.md
---
