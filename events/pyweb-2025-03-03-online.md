---
date: 2025.03.03
title: Git is a great design
register: https://www.meetup.com/pyweb-il/events/305280395/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/git-is-a-great-design-virtual.md
    time: "20:30"
---
