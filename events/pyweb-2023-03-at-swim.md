---
date: 2023.03.13
title: "PyWeb-IL #102 - March 2023: VisiData and test architecture"
register: https://www.meetup.com/pyweb-il/events/291223193/
group: PyWeb
number: 102
short_summary: "VisiData and test architecture"
location:
  name: Swimm
  url: https://swimm.io
  address: "Daniel Frisch St 3, 6th floor, Tel Aviv-Yafo"
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/visidata-by-ram-rachum.md
  - presentation: presentations/test-architecture-can-make-or-break-your-project.md
---
