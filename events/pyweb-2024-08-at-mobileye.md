---
date: 2024.08.07
title: "PyWeb-IL #108 - August 2024"
group: PyWeb
number: 108
register: https://www.meetup.com/pyweb-il/events/301896000/
id: pyweb-108
location:
  name: MobileEye
  url: https://www.mobileye.com/
  address: Mordekhai Anielewicz St 52, Tel Aviv-Yafo
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/implementing-the-composite-design-pattern-in-python-by-asher-sterkin.md
  - presentation: break
  - presentation: presentations/accelerating-parquet-file-filters-via-row-groups.md
---
