---
date: 2022.05.02
title: "PyWeb-IL #97 - May 2022: Federated Learning and Regular Expressions"
register: https://www.meetup.com/PyWeb-IL/events/284587211/
group: PyWeb
number: 97
short_summary: "Federated Learning and Regular Expressions"
location:
  name: Online
schedule:
  - presentation: presentations/federated-learning-by-tal-einat.md
    time: "18:00"
  - presentation: presentations/stand-back-i-know-regular-expressions.md
---
