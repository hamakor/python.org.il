---
date: 2025.01.19
title: Deep Neural Network as SQL+Python
register: https://www.meetup.com/pyweb-il/events/305381304/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/deep-neural-network-as-sql-and-python.md
    time: "18:00"
---
