---
date: 2015.10.12
title: "PyWeb-IL #47 - October 2015"
register: https://www.meetup.com/PyWeb-IL/events/224395519/
group: PyWeb
number: 47
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/django-test-all-the-things.md
    time: "18:00"
  - presentation: presentations/indepth-tox-managing-multiple-test-environments.md
  - presentation: presentations/how-i-learned-to-use-the-computer-efficiently.md
---
