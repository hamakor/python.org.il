---
date: 2015.02.02
title: "PyWeb-IL #42 - February 2015"
register: https://www.meetup.com/PyWeb-IL/events/219807718/
group: PyWeb
number: 42
location:
  name: NA
  url: https://python.org.il/
  address: "Rothschild 22 · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/webrtc-explain-the-basic-principles-of-webrtc.md
    time: "18:00"
  - presentation: presentations/elastic-search-aso-a-realtime-analytics-engine.md
  - presentation: presentations/combi-combinatorics-pacakge-for-python.md
---
