---
date: 2017.06.05
title: "PyWeb-IL #65 - June 2017"
register: https://www.meetup.com/PyWeb-IL/events/239963272/
group: PyWeb
number: 65
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/zappa-a-framework-for-developing-microservices-on-aws.md
    time: "18:00"
  - presentation: presentations/introduction-to-sysdig.md
  - presentation: presentations/fighting-the-curse-of-knowledge.md
  - presentation: presentations/pyparsing-or-the-case-for-monads.md
---
