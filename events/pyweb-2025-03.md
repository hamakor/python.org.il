---
date: 2025.03.24
title: "PyWeb-IL #114 - March 2025"
register: https://www.meetup.com/pyweb-il/events/305714893/
group: PyWeb
number: 114
location:
    name: Via
    url: https://ridewithvia.com/
    address: Midtwon Tower, Derech Menachem Begin 144, Tel Aviv-Yafo
schedule:
  - presentation: mingling
    time: "18:00"
---
