---
date: 2024.09.16
title: PyCon Israel 2024 - Conference
register: https://pycon.org.il/2024/
group: PyConIL
location:
    name: Cinema City
    url: https://www.cinema-city.co.il/location/1
    address: Gelilot
schedule:
  - presentation: presentations/leveraging-python-for-real-time-image-processing.md
  - presentation: presentations/malicious-needle-in-a-haystack-pypi-security-pitfalls.md
  - presentation: presentations/teach-your-old-code-new-tricks-automating-code-quality-in-large-projects.md
  - presentation: presentations/reaching-one-million-hands-for-israeli-advocacy.md
  - presentation: presentations/secimport-tailor-made-ebpf-sandbox-for-python-applications.md
  - presentation: presentations/beyond-kmeans-using-llms-to-improve-text-clustering.md
  - presentation: presentations/designing-a-plugin-architecture-in-python.md
# https://cfp.pycon.org.il/pycon-2024/talk/YQZD9U/
  - presentation: presentations/accelerating-ml-development-with-multi-modal-datasets-leveraging-python-parquets-and-daft.md
  - presentation: presentations/hacking-the-python-import-system.md
  - presentation: presentations/it-is-all-about-the-db-our-journey-between-databases-to-accommodate-100k-events-per-second.md
  - presentation: presentations/empowering-the-next-generation-teaching-python-to-young-girls-through-alicecode.md
  - presentation: presentations/no-more-dependency-nightmares-the-wix-way-for-a-healthy-and-stable-python-platform.md
  - presentation: presentations/how-to-make-your-backend-roar.md
  - presentation: presentations/unlocking-pythons-ast-the-metaprogramming-superpower-you-didnt-know-you-had.md
  - presentation: presentations/reducing-your-memory-footprint-by-75-with-6-lines-of-code.md
  - presentation: presentations/letting-ai-steal-my-job-i-am-not-even-mad-this-is-amazing.md
  - presentation: presentations/unblocking-the-loop-mastering-pythons-io-loop-monitoring.md
  - presentation: presentations/making-python-100x-faster-with-less-than-100-lines-of-rust.md
  - presentation: presentations/the-hitchhikers-guide-to-advanced-python-monitoring.md
  - presentation: presentations/unit-testing-llm-agents.md
  - presentation: presentations/how-we-deleted-a-dozen-files-and-10000-lines-of-code-and-got-control-of-our-airflow-dags.md
  - presentation: presentations/let-json-schema-and-pydantic-write-your-data-models.md
  - presentation: presentations/logger-info-at-pycon-israel-2024.md
  - presentation: presentations/python-bad-cryptography-habits.md
  - presentation: presentations/cli-ci-diy-crafting-python-based-dev-tools-in-a-devops-vacuum.md
  - presentation: presentations/10-ways-to-shoot-yourself-in-the-foot-with-tests.md
  - presentation: presentations/how-to-created-addicted-to-python.md
---
