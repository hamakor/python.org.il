---
date: 2016.02.01
title: "PyWeb-IL #50 - February 2016"
register: https://www.meetup.com/PyWeb-IL/events/227780028/
group: PyWeb
number: 50
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/automate-cli-apps-with-bats.md
    time: "18:00"
  - presentation: presentations/ld-reinstating-linux-distribution-identification-and-information-gathering-in-python.md
  - presentation: presentations/django-oscar-e-commerce-framework.md
---
