---
date: 2016.07.04
title: "PyWeb-IL #54 - July 2016"
register: https://www.meetup.com/PyWeb-IL/events/231814442/
group: PyWeb
number: 54
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/hypothesis-simpler-and-more-powerful-test-cases.md
    time: "18:00"
  - presentation: presentations/descriptors-and-the-python-dot-operator.md
  - presentation: presentations/how-slack-transformed-our-company.md
---
