---
date: 2025.01.22
title: "From Migrations to Tests: Alembic-powered DB Consistency"
register: https://www.meetup.com/pyweb-il/events/305252908/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/from-migrations-to-tests-alembic-powered-db-consistency-virtual.md
    time: "20:30"
---
