---
date: 2020.09.07
title: "PyWeb-IL #87 - September 2020"
register: https://www.meetup.com/PyWeb-IL/events/272609396/
group: PyWeb
number: 87
location:
  name: Online
schedule:
  - presentation: presentations/how-whatsapp-works-by-tal-shahaf.md
    time: "18:00"
  - presentation: presentations/a-python-quiz-by-miki-tebeka.md
---
