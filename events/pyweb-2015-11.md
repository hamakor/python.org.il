---
date: 2015.11.02
title: "PyWeb-IL #48 - November 2015"
register: https://www.meetup.com/PyWeb-IL/events/226076279/
group: PyWeb
number: 48
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/rabbitmq-and-using-message-queues-in-general.md
    time: "18:00"
  - presentation: presentations/wagon-creates-compiled-offline-installable-wheel-base-python-module-archives.md
  - presentation: presentations/build-a-better-backend.md
---
