---
date: 2017.11.20
title: Your First Open Source contribution
register: https://www.meetup.com/PyWeb-IL/events/244925304/
group: PyWeb
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/your-first-open-source-contribution.md
    time: "18:00"
---
