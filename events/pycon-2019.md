---
date: 2019.06.02
title: PyCon Israel 2019
register: https://pycon.org.il/2019/
group: PyConIL
location:
    name: Wohl Center
    url: https://wohl-center.com/wohl-conventions-center/
    address: Bar-Ilan University
schedule: []
---

# 2019.06.02-06 [PyCon IL 2019](https://il.pycon.org/2019/)
# * June 2 Django Girls
# * June 3-4 Main Conference
# * June 5 Workshops Day
# * [Videos from PyConIL 2019](https://www.youtube.com/channel/UC-SbPEAZ4Ik2cowdR_Wyfag)


