---
date: 2017.12.04
title: "PyWeb-IL #71 - December 2017"
register: https://www.meetup.com/PyWeb-IL/events/245071494/
group: PyWeb
number: 71
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/python-needs-a-better-testing-framework.md
    time: "18:00"
  - presentation: presentations/playing-with-cython.md
  - presentation: presentations/pipenv-the-future-of-python-dependency-management.md
---
