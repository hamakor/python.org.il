---
date: 2024.09.03
title: "PyWeb-IL #109 - September 2024"
register: https://www.meetup.com/pyweb-il/events/301692462/
group: PyWeb
number: 109
location:
  name: Tenable
  url: https://www.tenable.com/
  address: 94 Yigal Alon St., Alon Tower 2, floor 17, Tel Aviv-Yafo
schedule:
  - presentation: mingling
    time: "18:00"
  - presentation: presentations/from-sluggish-to-speedy-by-daniel-balosh.md
  - presentation: break
  - presentation: lightning-talks
  - presentation: presentations/marimo-a-new-python-notebook-by-alon-levy.md
  - presentation: presentations/dicom-by-gal-goldner.md
  - presentation: presentations/a-pagination-that-scales-with-your-data-by-naum-raviz.md
---
