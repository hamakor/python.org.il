---
date: 2020.07.06
title: "PyWeb-IL #86 - July 2020: Bauhaus and class optimization"
register: https://www.meetup.com/PyWeb-IL/events/269345861/
group: PyWeb
number: 86
short_summary: "Bauhaus and class optimization"
location:
  name: Online
schedule:
  - presentation: presentations/bauhaus-software-architecture.md
    time: "18:00"
  - presentation: presentations/make-your-classes-faster-and-leaner.md
---
