---
date: 2025.03.10
title: From Pandas to Polars in 60 minutes
register: https://www.meetup.com/pyweb-il/events/305967979/
group: PyWeb
location:
  name: Online
schedule:
  - presentation: presentations/from-pandas-to-polars-in-60-minutes.md
    time: "20:30"
---
