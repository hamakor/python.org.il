---
date: 2017.10.02
title: "PyWeb-IL #69 - October 2017"
register: https://www.meetup.com/PyWeb-IL/events/243515877
group: PyWeb
number: 69
location:
  name: Google for Startups Campus
  url: https://google.com/
  address: "34th floor electra tower · Tel Aviv-Yafo"
schedule:
  - presentation: presentations/screw-dsls.md
    time: "18:00"
  - presentation: presentations/intro-to-vue-js.md
  - presentation: presentations/lets-answer-some-interesting-job-interview-questions.md
---
