## Title:

Start with `[Talk]` and make this the title of the issue

## Description:

Longer free text.

## Length:

Please tell us the expected length of the presentation. We prefer presentations that are 20-40 minutes long. If you are a new speaker it is better to start with a 20 min presentation.

## Language:

Hebrew or English   (we prefer Hebrew, but accept English if you are more comfortable in it)

## On-line or in-person?

Would you prefer to give your presentation on-line where we can record and publish or off-line where you can get face-to-face with the audience?

## Date?

Do you have any preference for which event?

## Who are  you?

Please write a few words about yourself. Include a link to your web site and to your LinkedIn account, if you have either of them. Best to write in 3rd person.

