---
name:  Noam Morey
linkedin: https://www.linkedin.com/in/noam-morey-73249919a/
homepage:
github:
gitlab:
Slides:
img: noam-morey.jpeg
---

I'm a Fullstack developer, with Nym for almost 3 years focusing mostly on the Backend and DB, which we've been running on Postgresql with RDS. My motto is, "I don't have time to not write tests!
