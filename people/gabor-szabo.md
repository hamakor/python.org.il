---
name: Gábor Szabó
homepage: https://szabgab.com/
github: szabgab
gitlab: https://gitlab.com/szabgab
Slides: https://slides.code-maven.com/
linkedin: https://www.linkedin.com/in/szabgab/
img: gabor-szabo.jpeg
---

Python and Rust trainer and consultant.

The organizer of the [PyWeb-IL](https://www.meetup.com/pyweb-il/), the [Rust in Israel](https://www.meetup.com/rust-in-israel/), and the [Code Mavens](https://www.meetup.com/code-mavens/) Meeetup groups.

The maintainer of the [python.org.il](https://python.org.il/) and the [rust.org.il](https://rust.org.il/) web sites.
