---
name: Netanel Stern
linkedin: https://www.linkedin.com/in/netanel-stern-00293b90
homepage:
github: nsh531
gitlab: https://gitlab.com/nsh531
Slides:
img: netanel-stern.jpeg
---

Netanel Stern is a 26-year-old cybersecurity expert and Python developer specializing in penetration testing and secure coding practices. Beyond his expertise in ethical hacking, he has advanced skills in Python development, focusing on creating deep neural networks (DNNs) powered entirely by SQL for data management. His innovative approach integrates artificial intelligence with robust database systems, making him a unique contributor to the fields of AI and data security.
Netanel is also actively contributing to open-source projects, showcasing his commitment to advancing technology for public benefit. Two notable projects include:


1. [xCOVID](https://github.com/nsh531/xcovid) – A project aimed at leveraging AI and SQL-based deep learning to analyze and manage data related to COVID-19.
1. [xSchizo](https://github.com/nsh531/xschizo) – A tool designed to aid in schizophrenia research by applying neural networks for data analysis and prediction.

In addition to his professional contributions, Netanel is authoring a beginner-friendly book on penetration testing. This comprehensive guide introduces essential tools, techniques, and Python programming for security applications, tailored for those new to the field.
For inquiries or collaboration, you can reach him via email at nsh531@gmail.com.

