---
name: Miki Tebeka
company:
  name: 353 Solutions
  url: https://www.353solutions.com/
linkedin: https://www.linkedin.com/in/mikitebeka/
github: tebeka
projects: https://tebeka.github.io/
img: miki-tebeka.png
---
