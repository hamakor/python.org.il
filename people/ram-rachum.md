---
name: Ram Rachum
github: cool-RR
linkedin: https://www.linkedin.com/in/ram-rachum-349b7a17/
img: ram-rachum.jpeg
blog:
  - title: Chipmunk Development
    url: https://chipmunkdev.com/
  - title: blog
    url: https://blog.ram.rachum.com/

---
