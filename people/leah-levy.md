---
name: Leah Levy
linkedin: https://www.linkedin.com/in/leah-levy-95338534/
homepage:
github: llevy1
gitlab: https://gitlab.com/LLevy1
Slides:
img: leah-levy.jpeg
---

Leah is a Data Scientist. In her day to day role she uses Data Science to streamline processes and save money. She loves sharing her knowledge and teaching others.

