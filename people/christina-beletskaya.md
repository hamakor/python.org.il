---
name: Christina Beletskaya
linkedin: https://www.linkedin.com/in/christina-beletskaya/
gitlab: krisbeletskaya
github: xtina-belle
img: christina-beletskaya.jpeg
---

A fullstack engineer working as a freelancer for almost 2 years now.
I love learning new things and passionate about sharing knowledge with others.

