---
name: Udi h. Bauman
github: dibaunaumh
twitter: https://twitter.com/dibau_naum_h
SpeakerDeck: https://speakerdeck.com/dibau_naum_h/
linkedin: https://www.linkedin.com/in/udibauman/
img: udi-h-bauman.jpeg
---
