---
title: "Unblocking the Loop: Mastering Python's I/O Loop Monitoring"
speakers:
    - people/nir-geller.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=YZ1fF6Lsi5o
---

Ever wondered why your I/O loop gets stuck in production? We'll explore Python's I/O loop with asyncio, share a real-life bug, explain async and await, and introduce our monitoring solution to prevent and resolve issues. Join us to learn more.

Ever wondered why your I/O loop gets stuck in production? In this talk we'll dive into the world of Python's asynchronous programming and I/O loop. We'll start with an introduction to event loops, using the built-in asyncio library. Then, we'll cover a real-life bug where a stuck I/O loop caused crashes and general slowness in our production environment. Afterwards We'll break down the mechanics of the I/O loop, explain terms like async, await, and coroutines. Finally, we'll talk about our solution for monitoring I/O loops in production, ensuring you can prevent and resolve issues. Join us to deepen your understanding of the I/O loop and learn new techniques for keeping your systems running smoothly.



