---
title: Django with Postgres materialized views
speakers:
    - people/shay-halsband.md
length: 20
---

* [slides](https://www.slideshare.net/shyhalsband/materialized-views-and-django)
