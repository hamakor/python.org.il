---
title: "Secimport: Tailor-Made eBPF Sandbox for Python Applications"
speakers:
    - people/avi-lumelsky.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=6DJNQtBJvLA
---

Introducing secimport, an eBPF-based sandbox toolkit for Python, enforcing specific syscalls per module. It traces, profiles, and enforces security policies, offering granular control and reducing attack surfaces, with negligible performance impact.

Introducing secimport, an eBPF-based sandbox toolkit designed to secure your Python applications by enforcing specific syscalls per module. Think of it as seccomp-bpf for individual modules in your code.

Traditional tools like seccomp and AppArmor enforce syscalls for the entire process, which isn't enough for Python's complex attack surface. Secimport traces the syscalls each module uses, creating a security profile that’s compiled into a lightweight eBPF script. This script enforces the profile during runtime, ensuring your application behaves as expected.

The need for secimport arises from the limitations of current security tools that apply blanket syscall restrictions, which fail to address the nuanced and varied needs of individual Python modules. Secimport solves this problem by allowing granular control at the module level, effectively reducing the attack surface and mitigating risks associated with supply chain attacks and insecure code execution. This level of fine-tuned control is only feasible with eBPF, a powerful technology that allows high-performance, real-time instrumentation and monitoring at the kernel level, enabling us to create robust, low-overhead security profiles for Python applications for the first time.

Secimport is perfect for preventing code execution attacks and securing your supply chain. It minimizes the risk of vulnerabilities by confining each module to only the syscalls it needs. Whether you’re dealing with AI models from insecure sources or just want an extra layer of defense, secimport has you covered with negligible performance impact.

This talk will dive into how secimport works, from tracing to policy enforcement, with practical examples and benchmarks. You’ll learn how to set it up in various environments, including Docker and macOS, and see real-world use cases that highlight its effectiveness.



