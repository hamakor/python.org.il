---
title: Matching Code to Runtime with BERT
speakers:
    - people/joseph-shamenzon.md
length: 30
language: English
---

Cloud traffic sniffing tools monitor traffic between services, aiming to uncover vulnerabilities. Matching this traffic to the API corresponding controllers detected in the code is challenging and crucial. This presentation will introduce an approach to achieving this match using the BERT language model with meaningful preprocessing steps.


