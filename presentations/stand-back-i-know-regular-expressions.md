---
title: Stand Back! I Know Regular Expressions
speakers:
    - people/aur-saraf.md
length: 50
video_he: https://www.youtube.com/watch?v=JyJDe04JPdY
---


[Slides](https://www.dropbox.com/s/g1b60z0zu6uvmos/Regex%20-%20PyWebIL.pdf?dl=0)
