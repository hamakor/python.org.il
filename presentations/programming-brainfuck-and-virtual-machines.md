---
title: Programming brainfuck and virtual machines
speakers:
    - people/aur-saraf.md
length: 30
---


* [slides](/talks/2016-11-07/brainfuck/brainfuck for pywel-il.pptx)
* [b.py](/talks/2016-11-07/brainfuck/b.py)
