---
title: Python Serialization landscape
speakers:
    - people/miki-tebeka.md
length: 40
video_he: https://www.youtube.com/watch?v=N1UKNHCLZKU
---

* [slides](https://tebeka.github.io/talks/serialize-pyweb/slides/index.html)
