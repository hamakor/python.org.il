---
title: Git is a great design
speakers:
    - people/aur-saraf.md
length: 45
video_he: https://www.youtube.com/watch?v=UbsqxWfJSsY
---

Git is an exceptionally great software design. By understanding its design, we will learn not only how to design great software (when it's possible), but also how to anticipate the existence and guess the name of every obscure git command or parameter, and become the go-to person in our team for git issues.

