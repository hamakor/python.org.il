---
title: SQL vs NOSQL in Django applications
speakers:
    - people/amit-nabarro.md
length: 45
---


Considerations, design and implementation decisions. The talk would include a hands-on workshop showing some examples working with MongoDB
