---
title: Python Types
speakers:
    - people/gabor-szabo.md
length: 30
language: Hebrew
---

Python is a very flexible language when it comes to variable types, but it allows for setting optional type-annotations.

You can mark all of your variables with types and then Python will promptly disregard your type-annotation.

So what is the value of these type annotation and how can you get to that value?

[slides](https://slides.code-maven.com/python/python-types-at-pyweb-2025-01.html)
