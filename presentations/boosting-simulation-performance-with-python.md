---
title: Boosting simulation performance with Python
speakers:
    - people/eran-friedman.md
length: 35
---

* [slides](https://www.dropbox.com/s/4ln0cwprkzt96hf/Boosting%20simulation%20performance%20with%20Python.pdf?dl=0)
