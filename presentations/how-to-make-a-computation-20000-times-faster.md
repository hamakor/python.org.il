---
title: Careful What You Search For! - or, how to make a computation 20,000 times faster
speakers:
    - people/shai-berger.md
length: 45
---

Use of regular expressions for searching and parsing text is very common, but it can be dangerous. Innocent-looking searches may turn out to be very slow on specially-crafted inputs, and if such inputs can be provided by users, that is called a REDoS vulnerability. This talk is about the causes of such slowness, possible fixes and prevention.
A Hebrew version of [the talk Shai gave in DjangoCon Europe 2024](https://pretalx.evolutio.pt/djangocon-europe-2024/talk/YFEMJ9/).


