---
title: Coroutines Magic! There is so much more to yield.
speakers:
    - people/noam-elfanbaum.md
length: 45
---

* [slides](https://github.com/noamelf/intro-to-python-coroutines-talk)
