---
title: Advanced topics in Django admin - custom forms, fields, inlines, Grapelli, and other amusing hacks
speakers:
    - people/michael-sverdlin.md
length: 40
video_he: https://www.youtube.com/watch?v=xmToBYGbvtw
---

* [slides](https://docs.google.com/presentation/d/1dNeBHuL-UtCEXsA8kl0Qw--mITxnvGLEfsUWSw6I-7Y/edit#slide=id.p)
