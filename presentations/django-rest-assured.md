---
title: Django REST-assured
speakers:
    - people/yehonatan-daniv.md
length: 10
---

* [slides](http://slides.com/ydaniv/django-rest-assured-intro#/)
* [on GitHub](https://github.com/ydaniv/django-rest-assured)
* [demo](https://github.com/ydaniv/django-rest-assured-demo)
