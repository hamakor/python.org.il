---
title: Creating a website on GitHub Pages using Markdown
speakers:
    - people/gabor-szabo.md
length: 100
language: Hebrew
video_he: https://www.youtube.com/watch?v=M33dNtC_6k4
---

In the recent years every time I taught Python to beginners (e.g. at the Weizmann Institute and some corporations) I preceded the Python part by a few hours of teaching the tools they will need for better coding experience and better tooling. Specifically I was teaching how to use git, and various features of GitHub including Markdown.

This presentation is the first part of a series of presentations that covert that material.

We'll start by creating a web site using Markdown and GitHub pages. This does not require and does not provide any programming knowledge yet, but it allows me to teach certain things in a way that lets the students become creative right from the beginning.

If you are interested, check out the "Home pages" of some of the students in the previous courses here and here and even the instances of the OSDC.

In this session you will create a (fee) web site on GitHub using only the web interface of GitHub. You will not need to install anything on your computer. After the roughly 1 hour long presentation you will have the opportunity to practice what you have learned and get help and feedback.

The second part of this series will be a week later.

* [GitHub Pages slides](https://slides.code-maven.com/github-pages/github-pages)
* [source of the slides](https://github.com/szabgab/slides)
* [The repository that was created during the event](https://szabgab.github.io/github-pages-demo-2025-02-07/)
