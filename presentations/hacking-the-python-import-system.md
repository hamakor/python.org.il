---
title: Hacking the Python Import System
speakers:
    - people/liad-oz.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=qULfwGEwRTY
---


Delve into the inner workings of Python's import system. Learn how standard imports are implemented, and how to extend the import statement to provide new functionality.

The Python import system is more dynamic and extensible than most people realize. While most users primarily use it to import built-in and installed packages, there are additional options available, such as importing from zip packages and C modules. Moreover, Python allows you to hook into the import machinery to write your own import procedures.
In this talk, we will explore how the import system works behind the scenes. We will cover finders, loaders, and sys.meta_path. How the default file importer works, explaining sys.path, PYTHONPATH and how relative imports work. We will see how these are implemented in Python with code snippets.
We will see 2 example usages of import hooks, one practical, the other, extremely ridiculous.

