---
title: Unix command line tips & tricks
speakers:
    - people/miki-tebeka.md
length: 30
---

* [slides](https://github.com/tebeka/talks/tree/master/cmdline-friendly)
