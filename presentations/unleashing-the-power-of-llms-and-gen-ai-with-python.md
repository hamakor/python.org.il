---
title: Unleashing the Power of LLMs & Gen AI with Python
speakers:
    - people/hila-weisman-zohar.md
length: 180
language: Hebrew
---

Explore Large Language Models & Generative AI in Python. Witness their power with demos & code examples. Unlock innovation by seamlessly integrating cutting-edge AI into your Python projects!


In this talk, we'll dive into the cutting-edge world of Large Language Models (LLMs) and Generative AI (Gen AI), exploring their transformative impact on various domains and their seamless integration with Python.

The LLM Revolution: We'll kick off by introducing LLMs, the cornerstone of modern AI systems. Through applied demonstrations, you'll witness the remarkable capabilities of models like GPT-4, showcasing their language understanding, generation, and task-solving prowess.

Gen AI in Action: Next, we'll delve into the realm of Generative AI, where LLMs are harnessed to create novel content, from text to images and beyond. You'll see how Python frameworks like Langchain and Llama-Index empower developers to build powerful Gen AI applications, unlocking new realms of creativity and productivity.

Running Example: Evolving NLP Solutions
To illustrate the rapid evolution of NLP techniques, we'll present a running example of the Document Summarization task, demonstrating how this specific problem was approached in the past and now in 2024 with the help of LLMs and Gen AI. This hands-on segment will highlight the stark contrast between traditional methods and the game-changing potential of modern AI systems.

Connecting to Python
Throughout the talk, we'll emphasize the deep integration of LLMs and Gen AI with Python, showcasing code examples and practical applications. You'll learn how to leverage Python libraries and frameworks to harness the power of these cutting-edge technologies, empowering you to build innovative solutions.

What You'll Gain:
- A comprehensive understanding of LLMs and Gen AI, their capabilities, and their impact on various domains.
- Practical experience with Python frameworks like Langchain and Llama-Index for building Gen AI applications.
- Insights into the evolution of NLP techniques and the transformative power of modern AI systems.
- Inspiration to explore and leverage LLMs and Gen AI in your own projects



