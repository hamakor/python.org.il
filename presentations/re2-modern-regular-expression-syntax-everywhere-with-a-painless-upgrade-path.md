---
title: "re2: modern regular expression syntax everywhere with a painless upgrade path"
speakers:
    - people/aur-saraf.md
length: 15
---

* [slides](/talks/2016-01-04/re3.pdf)
