---
title: API Star - Next generation framework!
speakers:
    - people/nir-galon.md
length: 30
---

* [slides](https://docs.google.com/presentation/d/e/2PACX-1vT1RIPJEUd2JCXaxD3Qbbe4AUtmv-BsFR4g4Bu_Ny1CCDHsYQqa66vV0oCR-z7BAIYNmHu_5FZp2iGh/pub?start=false&loop=false&delayms=3000)
* [code](https://gist.github.com/nirgn975/9c84175deaad39b1d28a22b6992ca6d4)
