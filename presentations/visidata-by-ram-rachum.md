---
title: "VisiData: A Vim-like tool for data science in the terminal"
speakers:
    - people/ram-rachum.md
length: 35
---

[slides](https://r.rachum.com/vd-deck)

