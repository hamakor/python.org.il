---
title: "ETL With Celery, MongoDB and RabbitMQ: Battle learned lesson"
speakers:
    - people/sivan-greenberg.md
length: 45
---

[slides](https://www.dropbox.com/s/usuonn1cmq1t8b3/ETL_PyWebIL_2018.pdf?dl=0)
