---
title: Your First Open Source contribution
speakers:
    - people/gabor-szabo.md
length: 120
---

There are many people at our meetings who would want to contribute to Open Source projects, but don't know where to start. At this meeting we'll have a mix of presentations, hands-on hacking and mentoring to bring you to your first Open Source contribution.

You'll need to bring your own computer and plenty of energy.

Presentation and mentoring by Gabor Szabo and additional volunteers.

* [Collab-dev slides](https://code-maven.com/slides/collab-dev/)
