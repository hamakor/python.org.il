---
title: Epistemology - reflections on what is a truthful representation of reality in the software world
speakers:
    - people/alon-nisser.md
length: 30
video_he: https://www.youtube.com/watch?v=ICHm9_sgaME
---

* [Slides](https://speakerdeck.com/alonisser/epistemology-and-disks)
