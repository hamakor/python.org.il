---
title: When infrastructure betrays you
speakers:
    - people/alon-nisser.md
length: 30
---

* [slides](https://alonisser.github.io/abstractions-talk/)
