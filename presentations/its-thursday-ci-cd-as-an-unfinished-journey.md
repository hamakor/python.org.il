---
title: It's Thursday! CI/CD as an unfinished journey
speakers:
    - people/alon-nisser.md
length: 30
language: Hebrew
---

It's Thursday afternoon, and you've got plans for this evening. You've just finished the feature. You push to main, and click deploy. OR DO YOU? let's talk about Thursday deployments and what they can teach us.

