---
title: Implementing the Composite Design Pattern in Python
speakers:
    - people/asher-sterkin.md
length: 30
---

The Composite Design Pattern is a powerful tool for treating collections and their individual elements uniformly. It's especially useful for tree-like data structures, such as file systems, cloud stack resources, or any system composed of smaller parts.

In this talk, I will demonstrate a generic implementation of this pattern in Python, showcasing a practical use case where it proved indispensable. This implementation serves as a great introduction to advanced Python metaprogramming.
The presentation is based on my Medium article: [Generic Composite in Python](https://medium.com/python-in-plain-english/generic-composite-in-python-4b88d6727ad0) and my talk at the [Pyjamas 2022 conference](https://youtu.be/aqe9m9pTc58).

It might be longer if there is interest in Python metaprogramming. We also can connect the discussion of Design Patterns to the current LLM and co-pilots hype.

