---
title: Python Profiling Made Easy
speakers:
    - people/christina-beletskaya.md
length: 20
---

In this session, we'll delve into the world of Python profiling, covering timeit, cPofile and snakeviz.
We'll explore how to leverage these tools to optimize our programs and make them run smoother and see practical examples.


### Agenda:

* What is Profiling?
    * Time and execution profiling ⌛️
    * Profiling tools 🛠️
    * Profiling with cProfile module 🕵️
    * Interpreting profiling results with snakeviz 🐍
    * Optimizing code with profiling insights 👩 💻
* Real life example 😲
* Conclusion
* Profiling in runtime 🏃 ♀️

## About Christina Beletskaya

Im Christina Beletskaya, a fullstack engineer working as a freelancer for almost 2 years now.
I love learning new things and passionate about sharing knowledge with others.


