---
title: "Python after Guido: The new governance model"
speakers:
    - people/tal-einat.md
length: 30
video_he: https://www.youtube.com/watch?v=MbIHQ-dnZbM
---

* [slides](https://www.dropbox.com/s/3iw2wz06befg2t5/Tal%20Einat%20-%20Python%20Governance.pptx?dl=0)
