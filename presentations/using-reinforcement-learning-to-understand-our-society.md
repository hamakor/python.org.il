---
title: Using Reinforcement Learning to understand our society
speakers:
    - people/ram-rachum.md
length: 50
video_he: https://www.youtube.com/watch?v=cyBu9WcBFhw
video_en: https://www.youtube.com/watch?v=WMVqYYarFkQ
---

* [Slides](https://docs.google.com/presentation/d/1uBV2VkCfds_K3Aj7JimK1aE22AJSmLVoOg5oR4pOJU4/)
* [Mailing list for updates](https://groups.google.com/g/ram-rachum-research-announce)

