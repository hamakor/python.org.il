---
title: Fighting the Curse of Knowledge - Insights on Mentoring Young Pythonists
speakers:
    - people/yotam-manor.md
length: 20
---

* [slides](https://slides.com/yotammanor/fighting-the-curse-of-knowledge/)
