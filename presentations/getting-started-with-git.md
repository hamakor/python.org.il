---
title: Getting started with Git
speakers:
    - people/gabor-szabo.md
length: 60
language: Hebrew
video_he: https://www.youtube.com/watch?v=Pt6iJCneAv4
---

In the recent years every time I taught Python to beginners (e.g. at the Weizmann Institute and some corporations) I preceded the Python part by a few hours of teaching the tools they will need for better coding experience and better tooling. Specifically I was teaching how to use git, and various features of GitHub including Markdown.

This presentation is the second part of a series of presentations that covert that material.

We base this session on the previous session where we created a web site on GitHub Pages using Markdown.

We'll install both the git command line client and VS Code on Windows.
(We'll use Windows for this as the majority of people use Windows despite the fact that I use Linux as my main computer.)
Then we'll clone the repository of our web site and make changes to it on our local computer pushing out the changes to GitHub.

We will not learn Python or write Python code in this session. Our focus will be entirely on learning the the basic features of git that will help us later when we start writing code as well.

After the presentation you'll have the opportunity to practice a bit and get immediate feedback.

* [git-scm](https://git-scm.com/)
* [VS Code](https://code.visualstudio.com/)
* [Notepad++](https://notepad-plus-plus.org/)

