---
title: RabbitMQ, and using message queues in general
speakers:
    - people/alon-nisser.md
length: 50
---

* [slides](http://alonisser.github.io/introduction-rabbitmq/)
