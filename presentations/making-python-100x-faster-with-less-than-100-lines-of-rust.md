---
title: Making Python 100x faster with less than 100 lines of Rust
speakers:
    -  people/ohad-ravid.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=Tc9MlJN2y04
---

We will take a small demo library which uses numpy, and improve its performance 100x using the py-spy sampling profiler and Rust

Python is awesome, but can be super slow

Even when using packages like numpy, we can still be limited by Python's general slowness.

In this talk, I'll walk you through taking a demo small library which uses numpy, and building a native extension to it with Rust, that improves performance by 100x, all while keeping the original API the same.




