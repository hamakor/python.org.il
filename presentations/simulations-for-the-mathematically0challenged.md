---
title: Simulations for the Mathematically Challenged
speakers:
    - people/miki-tebeka.md
length: 40
video_he: https://www.youtube.com/watch?v=elC91aogcJw
---

* [slides](https://github.com/tebeka/talks/tree/master/pyweb-sim)
