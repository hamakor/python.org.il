---
title: "From Sluggish to Speedy: A Developer's Guide to Taming MySQL Performance"
speakers:
    - people/daniel-balosh.md
length: 40
language: English
video_en: https://www.youtube.com/watch?v=uwwdm3Ytams
---

Based on internal presentation that can be seen [here](https://drive.google.com/file/d/1uUkZ6r1QeOkRVjalKs8FHM3Esj1dDsR6/view)

Django, Flask, and other Python applications accessing MySQL need to prioritize performance to meet business and customer needs.
When your MySQL database starts to drag, your users feel the pain. This talk will equip you with the essential tools and strategies to diagnose and tackle common MySQL performance bottlenecks.

We'll go beyond basic query optimization and explore how to analyze live data using powerful techniques like percentile metrics, slow query logs, and pattern matching. Discover common pitfalls like N+1 queries and excessive data retrieval, and learn practical solutions for optimizing your application's interaction with the database.

This session is packed with real-world examples and actionable advice tailored for Django, Flask, and other Python ORM users, empowering you to transform your MySQL database from a sluggish bottleneck to a performance powerhouse.

### About Daniel:

I'm a software engineer with over 10 years of experience building reliable systems. I'm currently a Principal Engineer at the CISO office of Eleos Health, where I focus on using technology to improve security and risk management. I care about putting code into production and making a business impact. I believe in a pragmatic approach, starting with the business need, validating it with testing, and then developing a practical solution. You can find my professional background and experience on [LinkedIn](https://www.linkedin.com/in/daniel-balosh)

[slides](https://offline.llc/lectures/#perf-sql)

