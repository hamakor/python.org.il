---
title: Intro to Typescript
speakers:
    - people/keren-kenzi.md
length: 30
video_he: https://www.youtube.com/watch?v=walgx_nu-_E
---

* [Slides](https://www.dropbox.com/s/5fley7jupvsfznb/TypeScript.pptx?dl=0)
