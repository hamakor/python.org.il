---
title: How to build a microservice with Python + FastAPI that will help to switch from RDS to DynamoDB and save costs.
speakers:
    - people/nikita-baryshev.md
length: 30
---

The microservice processed all requests between different clients and DDB. In addition to this, during the transfer period, both RDS and DDB were supported before the full switch to DDB.
I can talk about the general approaches I used to build this microservice, how I worked with the legacy code, monitoring, and what was the outcome.
Also, I will give a summary of all the pros and cons I faced and things that you could do better from the beginning.

I'm a full-stack developer currently working at Check Point in Tel Aviv. My stack is Angular + Python (Flask, FastApi). I'm also interested in web accessibility.

