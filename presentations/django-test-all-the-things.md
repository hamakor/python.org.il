---
title: Django - Test all the things. Unittests, functional tests and stress testing.
speakers:
    - people/michael-sverdlin.md
length: 45
---

* [slides](/talks/2015-10-12/Testing Python.pptx)
