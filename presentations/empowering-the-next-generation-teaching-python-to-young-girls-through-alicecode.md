---
title: "Empowering the Next Generation: Teaching Python to Young Girls through AliceCode"
speakers:
    - people/shani-bendor.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=Jthhc4OW5U8
---



Join me as I present my experience of teaching Python to 11-year-old girls through AliceCode’s program. Discover which concepts were challenging for them, examples of the coding exercises used, samples of their projects, and additional insights.

How do you teach 11-year-old girls Python? AliceCode is a non-profit organization whose goal is to train and empower young girls, from the fourth grade and up, in the software field. Through AliceCode, I had the privilege of teaching Python programming to young girls from various parts of the country via Zoom in their first steps into the world of coding.

During the session I will speak in-depth about the insights I gained in the process of teaching Python to young girls, including an overview of the program’s syllabus, which concepts were the hardest for them to understand, examples of the coding exercises and questions used during lessons, and samples of the students’ completed projects including code snippets.

I will also briefly share my firsthand observations of the outcomes and impact of early exposure to coding, and shortly discuss how I believe parents can motivate their children, specifically girls, to pursue and stay in computer science.


