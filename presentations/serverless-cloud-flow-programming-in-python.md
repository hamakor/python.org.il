---
title: Serverless Cloud Flow Programming in Python
speakers:
    - people/asher-sterkin.md
length: 40
---

* [slides](https://www.dropbox.com/s/awale353fz9usem/Serverless.pdf?dl=0)
