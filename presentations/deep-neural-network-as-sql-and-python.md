---
title: Deep Neural Network as SQL+Python
speakers:
    - people/netanel-stern.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=9I5-17rOcr0
---

Short Outline: DNN with SQL + Python using Built-in SQL Functions


1. Data Preprocessing in SQL:
    * Use SQL's built-in functions (SUM(), AVG(), ROUND(), etc.) to perform calculations directly in the database.
1. Fetch Processed Data in Python:
    * Execute SQL queries with processed results using libraries like pandas and sqlalchemy.
1. Feature Preparation in Python:
    * Split the SQL output into features (X) and targets (y), normalize data, and handle missing values.
1. Build and Train DNN Model:
    * Create a neural network with libraries like TensorFlow/PyTorch.
    * Train using preprocessed features as input.
1. Store Results in SQL:
    * Save predictions or results back into the database for further analysis.

