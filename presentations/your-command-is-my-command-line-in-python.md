---
title: "Your command is my command (Command line in Python)"
speakers:
    - people/yehuda-deutsch.md
length: 30
video_he: https://www.youtube.com/watch?v=ViwcFLYuXa8
topics:
    - cli
---

* [slides](https://uda.gitlab.io/talks/2019-07-pyweb-il-cli/)
