---
title: "Unlocking Python's AST: The Metaprogramming Superpower You Didn't Know You Had"
speakers:
    - people/yishai-zinkin.md
length: 20
language: English
video_en: https://www.youtube.com/watch?v=896nBC5hzV0
---

Discover Python's Abstract Syntax Tree (AST), and learn how this fundamental data structure simplifies code analysis and metaprogramming through real-life examples.

At first glance static analysis of code may seem daunting. Recognizing patterns in code as text is second nature to us, but attempting this programmatically can be highly cumbersome. Yet, this incredibly useful capability is a cornerstone of many Python tools we rely on to enhance our code and preemptively catch potential bugs. This is where the Abstract Syntax Tree (AST) comes in - a powerful data structure that Python uses to represent code, making many challenges of code analysis and metaprogramming much simpler.

In this talk, we will delve deep into Python's AST, offering you a fresh perspective on the syntax of Python and programming languages in general. Through practical examples, you'll learn how to implement static analysis and metaprogramming using AST, empowering you to harness this powerful tool yourself.

