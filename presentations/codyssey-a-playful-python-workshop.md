---
title: Codyssey - a playful Python workshop
speakers:
    - people/daniel-anderson.md
length: 180
language: English
---

We all know about the Odyssey and all those mythical heroes...
But behind every successful hero sits a programmer, trying to figure out WHY THE F*** won't the hero's boat move in the right direction.

Welcome aboard, hero-coder.

In this workshop you'll hone your Python skills by writing agents that autonomously play games! Compete to solve the most games in 3 hours.

How it works:
There will be 9 games where you'll write code that runs dozens of times a second and decides which action to take next. The games vary in complexity and familiarity - you'll meet snake, pong 3D, and a host of other games. Be the first to solve them all!

(Be sure to bring a laptop)

