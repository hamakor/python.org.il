---
title: "\"The strangler pattern\" dealing with legacy rewrites"
speakers:
    - people/alon-nisser.md
length: 10
---

* [slides](https://alonisser.github.io/strangler-pattern-talk/#1)
