---
title: Using Jenkins as a CI for Python-based projects
speakers:
    - people/gabor-szabo.md
length: 30
---

* [slides](https://slides.code-maven.com/jenkins/)
