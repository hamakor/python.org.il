---
title: Variable scope & closure
speakers:
    - people/miki-tebeka.md
length: 45
---


[slides](https://github.com/tebeka/talks/tree/master/pyweb-scope)
