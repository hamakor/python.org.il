---
title: Kubernetes CPU- Requests, limits and noisy neighbors
speakers:
    - people/alon-nisser.md
length: 30
video_he: https://www.youtube.com/watch?v=A0kZorlt17s
---

* [Slides](https://docs.google.com/presentation/d/e/2PACX-1vQFm6VDB5sPU4018mpMO-RSnjgTBG79YcFM_dTlJ8-i7BBF5zUOap9145_t-cqB05ffM9yJfg9pgaaP/pub?start=false&loop=false&delayms=10000)

