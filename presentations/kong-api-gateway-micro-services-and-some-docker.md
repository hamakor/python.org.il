---
title: Kong (API gateway), micro-services and some docker
speakers:
    - people/noam-elfanbaum.md
length: 30
---

* [slides](https://github.com/noamelf/kong-talk)
