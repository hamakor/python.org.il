---
title: Testing with PyTest
speakers:
    - people/gabor-szabo.md
length: 120
---


* [slides](https://code-maven.com/slides/python-programming/testing-with-pytest)

Writing tests is one of key features of Extreme Programming and it is a requirement of Continuous Integration and Continuous Deployment. Not only helps it organizations develop applications faster, it also makes the life of the programmers more pleasant.

In this workshop you'll learn how to write tests for Python modules. After the introductory presentation first you'll get a number of simple exercises to experiment with and then we are going to find real Python modules and write tests for them.

This is a hands-on workshop that takes about 3 hours based on my longer Python testing course.


Bring your own computer!

Instructor: Gabor Szabo

Please fill out this survey (3 min) to make it easier for me to understand the background of the audience:
https://docs.google.com/forms/d/e/1FAIpQLSfo-GsiSZPqWyb0ht6xoX4wDHJcyg_stXTubmNqPyRi-Tg7nw/viewform?usp=sf_link
