---
title: Scientific debugging
speakers:
    - people/noam-elfanbaum.md
length: 10
---

* [slides](https://github.com/noamelf/debugging-with-the-scientific-method/blob/master/Debugging-with-the-scientific-method.ipynb)
