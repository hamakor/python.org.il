---
title:  Using Streamlit to easily create interactive web apps and deploy machine learning models
speakers:
    - people/leah-levy.md
length: 20
language: English
---

Discover how to quickly turn your Python scripts into interactive web apps using Streamlit. This session will cover key features like visualisations, widgets, and deployment, empowering you to create user-friendly interfaces with minimal effort.

[slides](/slides/streamlit-2025.01.pptx)

