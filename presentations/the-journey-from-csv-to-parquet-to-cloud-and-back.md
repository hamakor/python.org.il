---
title: The journey from CSV to parquet to cloud and back.
speakers:
    - people/igor-mintz.md
length: 40
language: English
video_en:
---

Where Igor Mintz discusses the advantages and behind the scenes of parquet files and shows a pipeline reducing data size and the integration of Polars with the cloud.

