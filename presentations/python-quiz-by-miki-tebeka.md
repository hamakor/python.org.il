---
title: A Python Quiz
speakers:
    - people/miki-tebeka.md
length: 20
---

Author of [Python Brain Teasers](https://pragprog.com/titles/d-pybrain/python-brain-teasers/).

I'll present several pieces of Python code and will ask the audience to guess the output of each one. After each piece of code I'll show the output and explain why it is that way.

