---
title: A Day Has Only 24+-1 Hours
speakers:
    - people/miroslav-sedivy.md
length: 30
language: English
video_en: https://www.youtube.com/watch?v=mHaz5laPyHE
---

* [slides](https://speakerdeck.com/eumiro/a-day-has-only-24-1-hours-israel-2021-01-04)
