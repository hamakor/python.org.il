---
title: 10 Ways To Shoot Yourself In The Foot With Tests
speakers:
    - people/shai-geva.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=k-vDmoPT84g
video_en: https://www.youtube.com/watch?v=Ub31Ae6S1BY
---

Tests can be great, but sometimes they're really not.
Getting it right can be tricky, and even when there are problems - it can be hard to know why.
We'll see how to avoid some common expensive mistakes, and how this can look in the Python ecosystem.

Almost every developer who’s worked with tests has encountered a test suite that caused a lot of pain.

Some of them just don’t protect us when we need them, some are flaky, some keep breaking because of unrelated changes, some take hours to debug whenever they fail.

And while every company is different, there are definitely patterns.
Many problems are the result of some common pitfalls that trap many teams.
These pitfalls might be common, but they're not easy to spot - small differences in test implementation can dramatically affect the result, and I’ve seen all of them happen in strong, capable, experienced teams. Most of these I fell into myself at least once.

In this session, we'll take a look at a selection of problematic testing choices, with examples that show these in the context of common Python frameworks and libraries. We'll discuss how to identify them, what problems they might cause and what alternatives we have so we can save ourselves the pain.


