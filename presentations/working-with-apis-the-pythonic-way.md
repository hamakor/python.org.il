---
title: Working with APIs the Pythonic way
speakers:
    - people/haki-benita.md
length: 40
---

* [slides](/talks/2017-03-06/Haki Benita - Working with APIs the pythonic way.pdf)
