---
title: Getting Started with gRPC
speakers:
    - people/miki-tebeka.md
length: 180
language: Hebrew
---

gRPC is a high performance open source RPC framework.
In this workshop we'll build a gRPC server and client, going from basics to advanced topics such as testing, streaming, interceptors and more.

gRPC is an open source RPC framework from Google.

It has become the default choice for internal server to server communication within companies.
gRPC is fast, secure, has a schema for incoming and outgoing messages, supports streaming and provides many more benefits.

In the course, we'll write a ride hailing service.
We'll build the server, the client and along the way discuss testing, streaming, security and many more subjects.
We'll also discuss operational topics such as using TLS, sharing protocol buffers definition and exposing gRPC as REST API.


