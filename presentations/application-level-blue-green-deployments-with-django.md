---
title: Application Level Blue/Green Deployments With Django
speakers:
    - people/yotam-manor.md
length: 30
---

* [slides](http://bit.ly/BlueGreenTalk)
* [demo app](https://github.com/yotammanor/bluegreenbooks)
