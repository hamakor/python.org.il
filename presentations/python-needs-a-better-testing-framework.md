---
title: Python Needs a Better Testing Framework
speakers:
    - people/noam-tenne.md
length: 30
---

* [slides](https://github.com/noamt/presentations/tree/master/we-need-a-better-testing-framework)
* [Nimoy](https://github.com/browncoat-ninjas/nimoy)
