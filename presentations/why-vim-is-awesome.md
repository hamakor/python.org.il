---
title: Why Vim is Awesome
speakers:
    - people/miki-tebeka.md
length: 30
---

* [slides](https://github.com/tebeka/talks/tree/master/vim-rocks)
