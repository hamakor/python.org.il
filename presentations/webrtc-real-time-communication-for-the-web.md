---
title: WebRTC - Real time communications for the web
speakers:
    - people/benny-daon.md
length: 30
video_he: https://www.youtube.com/watch?v=-G_qleGWptA
---

* [Slides](https://slides.com/daonb/deck)
* [App](https://www.terminal7.dev/)

