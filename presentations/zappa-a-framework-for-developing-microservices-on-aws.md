---
title: Zappa, a framework for developing microservices on AWS
speakers:
    - people/ricardo-moreno.md
length: 30
---

* [slides](https://ricardinho.github.io/zappa_talk/)
