---
title: Read Code Every Day
speakers:
    - people/aur-saraf.md
length: 30
---

[Slides](https://www.dropbox.com/scl/fi/5tnw6klwzojyh0rut3hr3/Read-Code-Every-Day.pdf?rlkey=5jzsnnfhoeg18wi4nk07egnt5&dl=0)
