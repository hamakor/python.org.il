---
title: Python async.io - From zero to hero
speakers:
    - people/eyal-balla.md
length: 40
video_he: https://www.youtube.com/watch?v=8XU3KTuDVZ0
---


* [Slides](https://github.com/eyalballa/pyweb-il-asyncio)
