---
title: Django settings with pydantic
speakers:
    - people/ruth-dubin.md
length: 20
---

I'll present how we implemented our Django settings, by using pydantic, which enables us to validate the configurable values and prevent runtime errors. In addition, we run health checks on some configuration parameters' custom types (like DB connection), to ensure the source availability, and provide the configurations' json schema for validating changes in the config (Can be used by AWS AppConfig).

Ruth is a Senior Python developer,working at [Bluevine](https://www.bluevine.com/) in the Core Backend team.

