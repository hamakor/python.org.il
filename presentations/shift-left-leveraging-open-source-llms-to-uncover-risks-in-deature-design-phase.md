---
title: Shift Left - Leveraging Open Source LLMs to Uncover Risks in Feature Design Phase
speakers:
    - people/arnon-dagan.md
length: 20
language: English
---

This talk explores the “shift left” strategy, employing open source LLMs to detect risks during the software design phase and create clear, explanatory text to enhance understanding and suggest countermeasures. During the talk we'll discuss tools to evaluate text quality and enhance LLM performance in production.


