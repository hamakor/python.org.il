---
title: "Frictionless Data: Lightweight standards and tooling to make it effortless to get, share, and validate data."
speakers:
    - people/paul-walsh.md
    - people/adam-kariv.md
length: 50
---

* [slides](https://hackmd.io/p/S1fZiX_he#/)
* See also [Frictionless Data on GitHub](https://github.com/frictionlessdata/)
