---
title: From Pandas to Polars in 60 minutes
speakers:
    - people/igor-mintz.md
length: 60
language: Hebrew
video_he:
---

A hands-on talk that will make you feel comfortable switching from [Pandas](https://pandas.pydata.org/) to [Polars](https://pola.rs/).
We'll explore a dataset step by step and discuss similarities and differences between the two packages. Experience with Pandas is recommended, but not mandatory.


