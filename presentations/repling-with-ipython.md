---
title: REPLing with IPython
speakers:
    - people/miki-tebeka.md
length: 30
---

* [slides](https://github.com/tebeka/talks/tree/master/repling-ipy)
