---
title: Playing with Cython
speakers:
    - people/tal-einat.md
length: 30
---

* [slides](https://taleinat.github.io/playing_with_cython/)
