---
title: The Hitchhiker's Guide to Advanced Python Monitoring
speakers:
    - people/may-walter.md
    - people/shani-armon.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=8gHmXwJazHo
---

In this lecture, we will explore the evolution of Python monitoring over the years, covering tools and techniques from sys.monitoring to import hooks, highlighting advancements and best practices in keeping your Python code in check.

Join us and time-travel across the evolution of Python monitoring mechanisms. We'll delve into history from dedicated tools like sys.monitoring to more advanced techniques such as ceval and import hooks. This session will provide a comprehensive overview of how monitoring practices have developed over the years, offering insights into the best practices for maintaining and debugging your Python code and the pros and cons of each approach. Whether you're a seasoned developer or new to Python, you'll gain valuable knowledge on how to keep your code running smoothly and efficiently without hurting performance or your dev velocity with tedious maintenance.


