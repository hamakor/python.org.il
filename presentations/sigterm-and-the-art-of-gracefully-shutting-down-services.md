---
title: SIGTERM and the art of gracefully shutting down services. A lighting talk
speakers:
    - people/alon-nisser.md
length: 10
---

* [slides](https://alonisser.github.io/sigterm-talk/#1)
