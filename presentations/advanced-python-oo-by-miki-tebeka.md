---
title: Advanced Python OO
speakers:
    - people/miki-tebeka.md
length: 45
---

In this talk we'll explore the more advanced Object Oriented features of Python.
We'll talk about:

* Attribute access and how you can change it
* `__slots__`
* Name mangling
* static and class methods
* Abstract base classes (abc and collections.abc)
* Speical methods such as `__len__`, `__getitem__`, `__iter__` and others
* Metaclasses

You can see the source code for this talk [here](https://github.com/tebeka/talks/tree/master/pyweb-oo).
