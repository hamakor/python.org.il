---
title: Let JSON Schema and Pydantic write your data models
speakers:
    - people/shahar-heller.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=mbfJD9n6Pac
---



Pydantic is the most widely used data validation library for Python, and JSON is the most common format for exchanging data.
Take those together, and you too will never worry about sending, serializing, or creating valid data structures again.

As someone who had the pleasure of trying and creating clients for an OpenAPI, I know that one of the most annoying things to do is to work with a pythonic OpenAPI client that is too generic, only giving you the bare minimum when it comes to sending requests, not having any validation when it comes to sending data to the server, and leaving it up to you to hardcode everything.

If you ever needed to maintain complex data models in your code for any reason, you know it’s a hard-to-manage task, from forgetting what models were added and where, to what dependencies each model has, having duplicated models, to just having a messy codebase that is impossible to navigate.

Modern problems require modern solutions, so why not use the most popular data validators?
You might already be familiar with JSON Schema from interacting with it in Postman, Swagger, Redoc, or any other API documentation that exists. It’s a way to define a template for how a JSON object should look. Pydantic, like its name suggests, is a pedantic library that lets you create data models but does not let you make any typing mistakes. Combining the two, we can create a generator, a validator and a serializer all in one.

In this talk, we will discuss:
1. JSON Schema and what we can do with it.
2. Why trying to structure complex data models in code is generally a bad idea.
3. How to generate an OpenAPI client using Python.


