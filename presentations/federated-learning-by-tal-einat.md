---
title: "Federated Learning: A promising new method for privacy-preserving ML (AI) modelling"
speakers:
    - people/tal-einat.md
length: 30
video_he: https://www.youtube.com/watch?v=mCu8T3FZjAQ
---

* [Slides](https://www.dropbox.com/s/bgl23qjn5jpbwqq/PyWeb-IL%20%2379%20Using%20Health%20Data%20from%20Around%20the%20World%20Without%20Compromising%20Privacy%20Federated%20Learning.pdf?dl=0)
