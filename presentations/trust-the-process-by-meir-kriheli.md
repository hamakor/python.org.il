---
title: tRust the process
speakers:
    - people/meir-kriheli.md
length: 45
video_he: https://www.youtube.com/watch?v=XygLhh1Kcq8
topics:
    - rust
---

* [slides](https://meirkriheli.com/talks/#trust-the-process)
