---
title: "Using Spark with python: some tips and patterns"
speakers:
    - people/alon-nisser.md
length: 30
video_he: https://www.youtube.com/watch?v=eEqVfy-aUq8
---


* [slides](https://docs.google.com/presentation/d/e/2PACX-1vR2D4ZNyCWv1AM9Bb_96o2yvahL0IXtGL7klaxLpVo87weO4QTO-IyNwP1Td56KbSr9dCKnb31lah6D/pub)
