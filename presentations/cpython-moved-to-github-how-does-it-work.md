---
title: CPython moved to GitHub, how does it work?
speakers:
    - people/tal-einat.md
length: 15
video_he: https://www.youtube.com/watch?v=rZAIsTK9-nM
---

* [slides](https://www.dropbox.com/s/72q2g5jiekbh647/CPython%20Workflow.pdf?dl=0)
