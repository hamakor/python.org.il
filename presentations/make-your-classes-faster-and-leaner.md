---
title: Make your classes faster and leaner
speakers:
    - people/eli-gur.md
length: 45
video_he: https://www.youtube.com/watch?v=-DLXKUOaR-s
---


* [slides](https://www.dropbox.com/s/yfmxi7jn5l3av1x/2020-07-06%20Eli%20Gur%2C%20Effective%20Python%20Classes.pdf?dl=0)
