---
title: Ordinal not in range - Python2 unicode
speakers:
    - people/benny-daon.md
length: 40
---

* [slides](https://nedbatchelder.com/text/unipain/unipain.html#1)
