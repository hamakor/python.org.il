---
title: The design and development of Choices in Django 3.0
speakers:
    - people/shai-berger.md
length: 30
video_he: https://www.youtube.com/watch?v=eJixjihG6II
---


* [Slides](https://shaib.gitlab.io/public/files/Django-choices-Djangocon-2020.pdf)

