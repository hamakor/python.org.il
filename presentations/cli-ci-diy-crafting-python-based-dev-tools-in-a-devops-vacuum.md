---
title: "CLI, CI, DIY: Crafting Python Based Dev Tools in a DevOps Vacuum"
speakers:
    - people/liubov-burtseva.md
    - people/uri-drori.md
length: 20
language: English
video_en: https://www.youtube.com/watch?v=Rgc4Z4da7yk
---

Dive into our Python-powered adventure of transforming a hodgepodge of bash scripts into a sleek, unified Python package for our entire CI toolkit. We'll share how our small team, armed with Python and determination, filled the DevOps void.

In this talk, we will be exploring how we embraced the fact that Valerann is a Python company and how we implemented it in everything that we do - even in our CI tools.

We use microservice architecture, manage 10+ AWS accounts, do weekly production releases, use various monitoring tools and actively develop the product.

Two years ago, our development tools where scattered amongst various off-the-shelf products and custom-built tooling in a variety of languages.

In the time since, we have fully switched over our development tools to be a singular Python package. This allows us to define containers, services, monitoring, alerting, testing and much more using the language all our developers are most comfortable with. Gone are the days of writing bash scripts, yaml definitions and json blobs!

We'll talk about what we had before, how is was implemented, how we did the same in Python:

* New repo scaffolding
* Monitoring and alerting (Grafana)
* Infrastructure (Terraform)
* Building containers
* AWS deployment


