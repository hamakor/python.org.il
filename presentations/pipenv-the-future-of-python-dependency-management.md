---
title: "Pipenv: The future of Python dependency management"
speakers:
    - people/nir-galon.md
length: 20
---

* [slides](https://docs.google.com/presentation/d/e/2PACX-1vTzr6aOFunDqTBbSkOBGZin5GqJN4QJLXXNZXLMfQQCXHfneiDx57vcwOmOUeuYVSW6e6iEETDkeWcX/pub?start=false&loop=false&delayms=3000)
