---
title: Introduction to Playwright with Python
speakers:
    - people/gil-zilberfeld.md
length: 420
language: Hebrew
---


Playwright's the cool kid on the block for web testing. Learn how it makes life easier, tests readable. Oh, and what you should know going in, the annoying stuff. Mostly exercises, some talking.

Playwright has become one of the main tools for testing web applications. In this packed 1-day intro, we'll learn how we can use it to automate and test web UI.

We'll see how to write, run and debug Playwright tests. We'll go over locators and expectations, the building blocks of Playwright, and also talk about the async aspect of the operations. We'll talk about refactoring the tests using the Page Object Model, and also see how we can use it for API testing.

On top of that, we'll discuss where Playwright fits into the testing work - what types of tests fit Playwright more, and when they can be replaced with simpler tests. We'll also see how to run the tests in CI environment.


