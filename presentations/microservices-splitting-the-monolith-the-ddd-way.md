---
title: "Microservices: Splitting the Monolith the DDD Way"
speakers:
    - people/erik-asepha.md
length: 40
---

* [slides](https://www.slideshare.net/ErikAshepa/microservices-splitting-the-monolith-the-ddd-way)
