---
title: Automate CLI apps with BATS
speakers:
    - people/alon-nisser.md
length: 10
---

* [slides](https://alonisser.github.io/Introduction-bats/)
