---
title: "Once, Twice, Chaos: Mastering Idempotency in Event-Driven Systems"
speakers:
    - people/avichay-attlan.md
length: 45
language: Hebrew
---

What happens if an event comes more than once? Have you ever heard the phrase "at least once"?

In the evolving world of cloud computing and event-driven architectures, we must adapt and master a new world of challenges.

Idempotency, the ability of handling duplicate events and processing them only once, is a challenging yet crucial art to master when building distributed services. The ability to perform retries and avoid unexpected side-effects and corrupted data is essential if you want to master supporting services at scale. Since those issues may occur in any event-driven system, it is fundamental to come prepared and know how to handle them.

In this session, we will explain what idempotency is, why it’s essential, and what the issues are when not handled correctly. We discuss also common scenarios to be aware of idempotency hazards, how to identify, handle, and avoid them. Finally, we will present the tools and common practices to create an idempotent system.

By the end of this talk, the audience will be familiar with the idempotency hazards and how to overcome them, highly improving their relability and resilience.




