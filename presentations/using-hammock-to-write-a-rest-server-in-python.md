---
title: Using Hammock to write a REST server in Python
speakers:
    - people/eyal-posener.md
length: 45
---

* [slides](https://docs.google.com/presentation/d/1ukQqZrsaohORukOo8T5JxdHvzhlxgDdZJFdoHZPIcQc/pub#slide=id.p)
