---
title: "Teach Your Old Code New Tricks: Automating Code Quality in Large Projects"
speakers:
    - people/dor-schwartz.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=AmqLp_JxQa0
---


Python’s nature often invites slip-ups: We’ll explore tools that prevent (some) bugs and improve code quality, discuss strategies for adding them to existing projects, and share tips for keeping your repo tidy and your fellow developers happy.

Ew, what’s that smell? Is it a non-typed, 15-argument, Python2-styled function, with a mutable-default argument and unreachable code again?

Your code is your kitchen: bugs hide best when it’s dirty🪳

Luckily, there are awesome tools to help reduce code smells, improving the quality, safety and consistency of your beloved Python code! 🧹

In the talk, we’ll explore a range of these tools, see how to fit them to your project’s needs, integrate them into IDEs and CI pipelines, choose the right rules to enforce, and apply them to your codebase without annoying your coworkers too much


