---
title: Python performance profiling
speakers:
    - people/noam-elfanbaum.md
length: 30
---

* [slides](https://docs.google.com/presentation/d/1J6xFYMnt8-esphMg-pXlikUc-_i7YP_FkcG6Gp2g1Ig/edit#slide=id.p3)
* [code](https://github.com/noamelf/profiling-python-talk)
