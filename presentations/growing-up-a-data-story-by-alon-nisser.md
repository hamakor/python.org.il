---
title: Growing up - A data story
speakers:
    - people/alon-nisser.md
length: 35
---


How Zencity grown from a single client in southern Israel to serving more than 130 million citizens On three continents - from the data perspective. We’ll talk about how we’ve started and the initial choices you need to make. And how we’re working on scaling a product from a single-digit number of clients to our current scale. About what happens when eventually the original setup doesn’t work anymore, new requirements appear, and the landscape changes. We’ll candidly discuss our stopgap measures and some of our missteps and wrong choices. We hope our experience will help others in their path.

[slides](https://docs.google.com/presentation/d/1S9Lm_vLGWr3I6q4JZHK2-H9YJckTTDfe4-RGVdbVr7I/)
