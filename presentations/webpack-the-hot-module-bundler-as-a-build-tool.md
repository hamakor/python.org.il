---
title: webpack - the hot module bundler as a build tool
speakers:
    - people/alon-nisser.md
length: 45
---

* [slides](https://alonisser.github.io/Intro-webpack-pywebIL/#1)
* [sample project](https://github.com/alonisser/webpack-reference-example)
