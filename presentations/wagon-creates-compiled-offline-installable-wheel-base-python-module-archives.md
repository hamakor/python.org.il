---
title: Wagon - Creates compiled, offline installable, wheel base Python module archives.
speakers:
    - people/nir-cohen.md
length: 15
---

* [slides](http://slides.com/nir0s/wagon#/)
