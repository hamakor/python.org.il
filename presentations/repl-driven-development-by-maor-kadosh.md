---
title: REPL-Driven Development – Adapting Lisp-style interactive development in Python
length: 15
speakers:
    - people/maor-kadosh.md
---

A common style of development in Lisps and Lisp-likes is writing code along a running stateful Lisp machine process, and evaluating bits of code every time a change is made.
In practice, I find it most useful when authoring scripts, and the workflow looks like this:

1. I load up some data; be it from a database or a remote site.
2. I'm not entirely sure how I use the data to achieve the goal of the script, so I start wrangling with it interactively, exploring its methods and nested fields.
3. I write some functions in my editor, evaluate them in the running IPython process (which holds the state, e.i. the data I fetched), and pass the data from the process' memory to the function I just evaluated.
4. Inspect the result, rewrite the function, inspect the data in memory some more, and so on

This isn't groundbreaking, and can be achieved with TDD as well (or just temporarily storing fetched data in a file). I just personally like this approach as it is very low friction.
I have written about this two years ago. Not a whole lot has changed: [Attempting REPL-Driven Development in Python](https://avocadosh.xyz/repl-driven-development-in-python.html)

I'm Maor Kadosh, I'm a relatively new DevOps person – been a Sysadmin at the military, working at the industry for two years. I have a soft spot for Python and scripting in general. Spare time is spent on both redundant hacking and fruitful hacking – and also making some music:)
[Website](https://avocadosh.xyz)


