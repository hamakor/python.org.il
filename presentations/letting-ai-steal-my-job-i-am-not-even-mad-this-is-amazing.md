---
title: Letting AI Steal My Job - I'm Not Even Mad, This Is Amazing
speakers:
    - people/carine-belle-feder.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=jm3EQq7oqEI
---

Reviewing code repetitively and documenting API endpoints can be tedious. In this talk, we’ll share how we wrote Python LLMs with Github actions and a few python SDKs for best practice advice on pull requests and automatic API documentation.

Being a software engineer is awesome. I love waking up in the morning and diving into code, planning new system components, or collaborating with my team to release great versions for our users. But doing the grunt work? I’ll pass. While I enjoy reviewing other developers’ architecture and learning from their approaches, being the team’s bottleneck, only to give the same repetitive “best practices” comments? No thanks. Speed coding new endpoints for great frontend capabilities? LOVE. Adding API endpoint documentation? Please, spare me the boredom.
We keep talking about “how amazing AI is going to transform our products,” but what really amazed me was the morning I realized I could turn Gemini into a sidekick that handles my dirty work.

In this talk, we’ll share how we used Gemini to offload tedious tasks and let us focus on the enjoyable parts of development. We’ll walk you through how we wrote a Python LLM that instantly provides best practice advice on GitHub pull requests and demonstrate how another LLM handles API documentation for us. We’ll also present other practical uses that might benefit your team. Additionally, we’ll discuss important considerations before starting, how we improved our processes, and what made the journey fun. This talk is perfect for anyone looking to take their first steps in writing LLMs, those who constantly seek to optimize their daily lives, and of course – Python enthusiasts.

