---
title: "asyncio: Asynchronous I/O, event loop, coroutines and tasks"
speakers:
    - people/meir-kriheli.md
length: 50
---

* [slides](http://meirkriheli.com/en/talks/#asyncio)
