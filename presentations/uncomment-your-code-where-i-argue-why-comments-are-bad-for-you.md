---
title: Uncomment Your Code
speakers:
    - people/miki-tebeka.md
length: 15
---

Where I argue why comments are bad for you and without them your code will improve. Includes some potentially life-saving advice.
