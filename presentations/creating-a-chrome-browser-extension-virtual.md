---
title: Creating a Chrome browser extension
speakers:
    - people/elad-silberring.md
length: 25
---

This lecture is for developers of all levels that want to know how to build a Chrome extension in no time!
We will zoom through the basics of the extensions manifest, build a silly extension an talk about the future.

