---
title: Enterprise integration patterns with messaging
speakers:
    - people/alon-nisser.md
length: 45
---

* [slides](https://alonisser.github.io/integration-patterns-messaging/)
