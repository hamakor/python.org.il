---
title: "Combi: combinatorics package for Python - the combinatorics package that I've written for Python."
speakers:
    - people/ram-rachum.md
length: 0
---

* [Combi](https://github.com/cool-RR/combi)
* [Documentation of Combi](https://combi.readthedocs.io/en/stable/)
