---
title: The Perfect Python Environment Setup
speakers:
    - people/david-bordeynik.md
length: 30
video_he: https://www.youtube.com/watch?v=duldKKruI3s
---

* [slides](https://drive.google.com/file/d/1-p05EQ6GAaBzlDN51-GwWGTmnOChD_J5/view)
