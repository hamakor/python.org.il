---
title: Gordon, a very convenient AWS lambda full automation framework + demo
speakers:
    - people/david-melamed.md
length: 0
---

* [slides](/talks/2016-12-05/David Melamed - Gordon.pdf)
