---
title: Supply chain security for open source models
speakers:
    - people/natan-katz.md
length: 30
language: English
---

Open Source models are used everywhere. As public entities that are downloaded from various websites they are nearly endowed with a certain level of risks. In this lecture we will cover some attack vectors that are unique for these models and describe some of the plausible attacks according to OWASP mL



