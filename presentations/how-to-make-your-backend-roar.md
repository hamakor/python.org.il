---
title: How to Make Your Backend Roar
speakers:
    - people/haki-benita.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=6HL4SKwIPK4
---

Developers who are not familiar with databases often dread them and treat them like blackboxes, but fear no more! In this talk I present advanced indexing technics to make your database faster and more efficient.

Indexes are extremely powerful and ORMs like Django and SQLAlchemy provide many ways of harnessing their powers to make queries faster and the database more efficient. In this talk I reveal the secrets of DBAs with some advanced indexing techniques such as partial, function based and inclusive B-Tree indexes, and who knows, maybe even some index types you never heard of before!

