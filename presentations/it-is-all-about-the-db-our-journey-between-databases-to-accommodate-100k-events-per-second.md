---
title: It's all about the db - our journey between databases to accommodate 100k events per second
speakers:
    - people/eyal-balla.md
length: 20
language: English
video_en: https://www.youtube.com/watch?v=u-CJFfPT0XY
---

We built a python application that ingests 100k events per second. This lecture will share our lessons learned using different dbs for this job.

Our python based system needs to handle more than 100k events per second. During the last two years we have tried, failed and evolved using multiple databases such as nosql, timeseries db and OLAP columnar dbs.
This lecture will share the lessons learned for running such a system in a production environment


