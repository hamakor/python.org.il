---
title: Python Pair Programming with TDD Workshop
speakers:
    - people/gabor-szabo.md
length: 120
---

Why work in pairs? Because you already know how to program alone. You can do that as much as you like at work or at home. Let's try something else now.

## Objectives

The primary objective of this session is to let you experiment with TDD and pair programming. To let you learn from each other without the pressure and fear involved in production code.

## Task

At the beginning of the session you are going to be presented by a task to work on. We will then divide into pairs and each pair will implement the task.

## Survey

It would be nice if you could spend a few minutes filling out this survey so I have a better understanding of the people who plan to attend the workshop
https://docs.google.com/forms/d/e/1FAIpQLSfo-GsiSZPqWyb0ht6xoX4wDHJcyg_stXTubmNqPyRi-Tg7nw/viewform?usp=sf_link

If you have already filled this last time, there are 2 additional questions. I think you can edit your responses. It would be nice if you updated those two questions.

People who fill the survey and leave their correct e-mail address will get a copy of my eBook about Collaborative Development.
Development Methodology

You are recommended to use TDD - Test Driven Development. You'll get help if you don't know how to start.

## Pairing

If you already have a person you'd like to work with that's fine, though I'd recommend our random pairing process to everyone. We'll put the names on pieces of paper. Put them in a hat and pull the names for the pairs from there.

## Working in pairs

We are going to experiment with the Driver-Navigator method starting with 5 min rotations. In this method one person is the driver who writes the code. The other person is the Navigator who tells the driver what to write.

The navigator explains the task at the highest possible abstraction that matches the knowledge and experience level of the driver.

Every 5 minutes the Driver and the Navigator switch roles.

As time progresses you will pay attention how you work and you can adjust.
The critical part is that you both pay attention.

## Requirements

We assume you already know Python at some level. (data structures, function, modules.)
You bring your own computer. (In the end we'll need one computer per pair.)

## Expectations

In order to be good at pair programming one needs to work that way for many hours. The two-three hours session we have will only give you a taste, but we'll repeat the experience again.
Retrospective

At the end of the session we are going to have a retrospective lead by Anat Alon from http://www.practical-agile.com/ . This is going to be a 30-40 min session. It will help us improve the workshop for the next meeting and it can help you see how to run retrospectives that can also help you in your own teams.

