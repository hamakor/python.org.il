---
title: Accelerating Parquet file filters via row groups
speakers:
    - people/uri-mogilevsky-shay.md
    - people/menachem-kluft.md
length: 39
---

You probably heard about the Apache Parquet file format, but how much do you really know about it? In this presentation you'll hear how Parquet is used in Mobileye to solve complex problems in the world of data.


