---
title: Exceptional Python - Error handling tips
speakers:
    - people/lior-mizrahi.md
length: 30
---

* [slides](https://github.com/liormizr/error_handling_talk)
