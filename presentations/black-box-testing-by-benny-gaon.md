---
title: Black box testing
speakers:
    - people/benny-daon.md
length: 30
---

* [slides](https://www.dropbox.com/s/wlikyuvbvdq7zqp/Micro%20Black%20Boxes.pdf?dl=0)
* [Code](https://www.dropbox.com/s/3y1z8rhix7kgo03/validator.tar?dl=0)
