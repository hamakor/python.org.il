---
title: Python bad cryptography habits
speakers:
    -  people/ran-bar-zik.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=7GFp2spi124
---

Cryptography is a vital part of application security, but the world of cryptography is dark and full of errors. We will talk about deprecated algorithms, poor key management, misuse of libraries, and best practices for secure implementation.

This session will discuss common mistakes developers make when implementing cryptography in Python. There are many. Through real-world issues and cases (yes, also embarrassing ones), we'll illustrate the consequences of these bad practices and provide practical recommendations for secure cryptographic implementation. A must-have for every developer that is working on a back-end,

