---
title: Going beyond MongoDB for your NoSQL purposes
speakers:
    - people/zohar-arad.md
length: 45
---


* [slides](http://zohararad.github.io/presentations/outgrowing-mongodb/)

There are quite a few new (read - next gen.) NoSQL DBs that fit use-cases usually filled by MongoDB, but with better scaling capabilities and less headaches (or different ones). So, the point is not to diss Mongo, but to show viable alternatives. Some examples are - ArangoDB, RethinkDB, FoundationDB, HyperDex and Elastic Search (there are many more but these come to mind at the moment).
