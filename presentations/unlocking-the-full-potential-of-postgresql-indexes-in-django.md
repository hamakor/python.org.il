---
title: Unlocking the full potential of PostgreSQL indexes in Django
speakers:
    - people/haki-benita.md
length: 40
video_he: https://www.youtube.com/watch?v=7F1pFy22Gyg
---

* [Slides](https://www.dropbox.com/s/ty8nymnjd84wdm3/Working%20with%20APIs%20the%20pythonic%20way.pdf?dl=0)

