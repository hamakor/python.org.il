---
title: Bauhaus Software Architecture
speakers:
    - people/dan-gittik.md
length: 30
video_he: https://www.youtube.com/watch?v=a0_5EkI4Qu0
---

* [slides](https://docs.google.com/presentation/d/1peVWsAk2mWNz0FBAuUmY48Yo7QRvulzGzqZgguzq3R0/edit)
