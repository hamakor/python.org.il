---
title: Test Automation with Robot Framework
speakers:
    - people/mark-geyzer.md
length: 20
---

* [slides](/talks/2017-09-04/RobotFramework.pptx)
