---
title: Reaching One Million Hands for Israeli advocacy
speakers:
    - people/kathrine-smoliansky.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=qoMWPc_2O9E
---

Discover ‘One Million Hands,’ an advocacy platform addressing Israel’s advocacy needs. Learn how we evolved from simple Python scripts to a robust platform using data science, machine learning, and Python to empower effective social media advocacy.

“In early October, we launched ‘One Million Hands’ to address the need for effective advocacy in Israel. Starting with simple Google Sheets and Python scripts, our project has evolved into a robust platform empowering individuals to impact social media conversations through optimal feedback on targeted content. Leveraging autonomous tools and a comprehensive data science pipeline, we enrich content behavior analysis by integrating data from multiple sources and applying machine learning models to tag content violations.

As a voluntary organization, we face challenges in long-term development, managing personnel, and creating robust, organized code. Our platform gathers crucial information from social networks, efficiently funneling data through our pipeline and data science services. Python has been instrumental in managing our systems, offering flexibility in volunteer contributions, knowledge preservation, and interfacing with various programming and cloud tools.

In the lecture, we will present the technical journey of ‘One Million Hands,’ focusing on Python’s role in data analysis, machine learning, and API development. Gain insights into practical applications of Python in a real-world project, the challenges we faced, and our solutions. A live demonstration of our platform will highlight how technology drives impactful advocacy.”

