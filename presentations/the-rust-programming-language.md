---
title: The Rust programming language
speakers:
    - people/chen-rotem-levy.md
length: 60
---

* [slides](https://github.com/chenl/talks/blob/master/20170102-rust/why-rust.org)
