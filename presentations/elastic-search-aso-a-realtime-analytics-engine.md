---
title: Elastic Search as a realtime analytics engine
speakers:
    - people/zohar-arad.md
length: 0
---

* [slides](http://zohararad.github.io/presentations/es-analytics/)

There's a true story here of how our company moved from HBase to ES and how we're using the latter to drive our analytics engine.
