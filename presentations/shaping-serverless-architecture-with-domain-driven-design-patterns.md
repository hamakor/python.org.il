---
title: Shaping serverless architecture with domain driven design patterns
speakers:
    - people/asher-sterkin.md
length: 30
---

* [slides](https://www.slideshare.net/AsherSterkin/shaping-serverless-architecture-with-domain-driven-design-patterns-py-webil)
