---
title: Malicious Needle in a Haystack - PyPi Security Pitfalls
speakers:
    - people/eugene-rojavski.md
length: 20
language: English
video_en: https://www.youtube.com/watch?v=ZvNuHKDyQXc
---



Every developer uses open-source packages and models. Only a fraction of us validate their security. This session will cover the supply chain security issues that Python developers face, show attacks, and recommend how to avoid them.

PyPi has 1.5 billion packages downloaded daily. This huge number is the perfect opportunity to disguise a malicious needle in the package haystack. Due to its popularity among Python developers, PyPi is also extremely popular among attackers. Same for HuggingFace, which gains its popularity with skyrocketed usage of models.
Attackers may target masses using techniques like typosquatting or perform targeted campaigns against maintainers of the top projects and even the whole developer communities.
One of the crucial skills that a Python developer must have nowadays is the responsible use of open-source dependencies.
The talk will focus on the issues that may happen to the developers and the way of avoiding them.

