---
title: "No more dependency nightmares: the Wix way for a healthy and stable python platform"
speakers:
    - people/roy-noyman.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=uK_p8shFJAA
---

Navigating Python dependencies can be a nightmare, especially with large frameworks like Apache Airflow. Join Roy as he shares the Wix way of keeping dependencies updated and stable,ensuring a true sleeping beauty in the world of software development

Any experienced Python developer knows that Navigating the complex landscape of Python dependencies can be akin to a nightmare, especially when dealing with large frameworks like Apache Airflow which has over 700 Python dependencies. In This talk Roy will share the Wix way of keeping dependencies updated and most importantly stable!

Join us for a deep dive into the world of Python dependency management with a focus on stability and sanity.
In this talk, Roy will uncover the challenges faced by developers dealing with an extensive array of Python dependencies, particularly within frameworks like Apache Airflow and proprietary in-house libraries. Drawing from his experience at Wix, where the Python ecosystem is rich and diverse, with more than 1,000 dependencies that serve ~200 Data Engineers, we share insights into our battle-tested methods for keeping dependencies up-to-date and rock-solid.
In this talk the audience will learn practical solutions and best practices for keeping their environment stable.
We will discuss several ways to effectively manage and handle custom dependencies in your python platform.

Whether you're an experienced Python developer or just starting out, join me to unlock the secrets to a tranquil dependency experience – a true sleeping beauty in the world of software development.



