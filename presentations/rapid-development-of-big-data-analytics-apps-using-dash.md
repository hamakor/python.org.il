---
title: Rapid Development of Big Data Analytics Apps using Dash
speakers:
    - people/roman-olshanskiy.md
    - people/david-katz.md
length: 40
---

[Slides](https://www.dropbox.com/scl/fi/53kuljisri1w0auopjvfs/Pyweb_Mobileye_slides.pdf?rlkey=0gnkk90oyruoe3qms1c9yc5ba&dl=0)


