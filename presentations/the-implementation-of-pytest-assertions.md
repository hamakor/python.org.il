---
title: The implementation of pytest assertions
speakers:
    - people/aur-saraf.md
length: 10
---

* [slides](https://speakerdeck.com/oinopion/dispelling-py-dot-test-magic)
