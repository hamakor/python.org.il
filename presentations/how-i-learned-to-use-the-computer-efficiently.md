---
title: How I learned to use the computer efficiently
speakers:
    - people/ram-rachum.md
length: 45
---

* [slides](/talks/2015-10-12/How to use the computer efficiently.pdf)
* [Power Point](/talks/2015-10-12/How to use the computer efficiently.pptx)
