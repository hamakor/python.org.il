---
title: Postgresql in Theory and Practice.
speakers:
    - people/amir-more.md
length: 50
language: English
video_en: https://www.youtube.com/watch?v=H2PnHxowNDg
---

* [Slides](https://www.dropbox.com/s/xuwmhbq2cc3i92p/PG%20Presentation%20for%20PyWebIL.pptx?dl=0)
