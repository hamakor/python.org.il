---
title: Git is a great design
speakers:
    - people/aur-saraf.md
length: 45
---

[Slides](https://www.dropbox.com/s/83n3ixj6xc95c9j/Aur%20git%20slides.pdf?dl=0)
