---
title: A Python quiz
speakers:
    - people/miki-tebeka.md
length: 30
video_he: https://www.youtube.com/watch?v=LC9UvJSpIDo
---

* [slides](https://github.com/tebeka/talks/tree/master/pyweb-quiz)
* The book: [Python Brain Teasers](https://gumroad.com/l/iIQT)

