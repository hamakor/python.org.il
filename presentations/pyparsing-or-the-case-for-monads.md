---
title: Pyparsing, or the case for monads
speakers:
    - people/yoav-luft.md
length: 25
---

* [slides](https://docs.google.com/presentation/d/1m2wg71UaT1OzDRrf6NhejBIKNFV-v2oOVExazse-flU/edit?usp=sharing)
* [Examples on GitHub](https://github.com/Luftzig/pycon-text-parsing-examples)
