---
title: DICOM (Digital Imaging and Communications in Medicine) standard
speakers:
    - people/gal-goldner.md
length: 10
---

DICOM (Digital Imaging and Communications in Medicine) standard, focusing on Python packages that are useful for working with the format and protocol, along with some examples.

[slides](/slides/DICOM-PyWeb-IL-109.pdf)


