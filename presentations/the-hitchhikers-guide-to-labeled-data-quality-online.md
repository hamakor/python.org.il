---
title: The Hitchhiker's Guide to Labeled Data Quality
speakers:
    - people/danielle-menuhin.md
length: 30
language: Hebrew
video_en:
---

Is GIGO wreaking havoc on your models? Trying to figure out how to make sure your annotated data is good enough? Found out that correcting bad labels is much more work than anticipated? Luckily for you, this is our forte in the Data Operations IL community – the people who manage human judgement operations on data. As community organizer and co-founder I come equipped with data labeling quality processes from across the tech world. This talk will define and expand your data labeling quality toolbox, which surprisingly might not necessarily include tooling development resources.


