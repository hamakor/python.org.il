---
title: How we built a versionator using datadog & puppet
speakers:
    - people/yotam-gafni.md
length: 30
---

* [slides](/talks/2017-02-06/How we built a versionator in 30m using datadog + puppet.pdf)
