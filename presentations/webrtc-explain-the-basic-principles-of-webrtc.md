---
title: WebRTC
speakers:
    - people/natan-brosztein.md
length: 0
---

Explain the basic principles of WebRTC, history and current status and show some demos and basic code. This includes concepts like codecs, SIP, RTP, SDP and ICE, NAT traversal, etc.
