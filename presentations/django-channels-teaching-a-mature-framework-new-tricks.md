---
title: "Django Channels: Teaching a mature framework new tricks (like websockets)"
speakers:
    - people/ronnie-sheer.md
length: 0
---

* [slides](/talks/2016-12-05/Rony Sheer - Django Channels.pdf)
