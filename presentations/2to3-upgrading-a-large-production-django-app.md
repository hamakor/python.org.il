---
title: "2to3: Upgrading a large, production Django app from Python 2.7 to 3.6."
speakers:
    - people/tal-einat.md
length: 40
---

* [slides](https://taleinat.github.io/pyweb-il-77-2to3/)

