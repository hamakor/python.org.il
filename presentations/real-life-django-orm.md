---
title: Real-life Django ORM
speakers:
    - people/max-braitmaiere.md
    - people/jenya-privalov.md
length: 60
---

* [slides](https://www.slideshare.net/MaximBraitmaiere/hexadite-real-life-django-orm)
