---
title: So you think you can print?
speakers:
    - people/miki-tebeka.md
length: 25
video_he: https://www.youtube.com/watch?v=wXZFy6HIRQA
---

* [slides](https://github.com/tebeka/talks/tree/master/pyweb-print)
* [Terminal log](https://github.com/tebeka/talks/blob/master/pyweb-print/terminal.log)
