---
title: Why is Go cool and how can you use it from Python
speakers:
    - people/miki-tebeka.md
length: 40
---


* [slides](https://github.com/tebeka/talks/tree/master/go-pyweb)
* [vimrc of Miki Tebeka](https://github.com/tebeka/talks/blob/master/go-pyweb/vimrc.vim)
