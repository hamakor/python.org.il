---
title: "From Migrations to Tests: Alembic-powered DB Consistency"
speakers:
    - people/noam-morey.md
length: 30
---

How can you make sure everyone in your org works with the same database schema?
How can you test all your database-interacting functions without elaborate setup?

In this talk we'll introduce Alembic, and how it organizes our DB migrations - letting us make, propagate, deploy, and rollback the DB schema as needed; and most importantly - test our code and our DB, all with the simplicity of standard source control.
