---
title: Cloudify - Managing a large, distributed, open-source Python project.
speakers:
    - people/nir-cohen.md
length: 60
---

* [slides](http://slides.com/nir0s/what-is-cloudify#/)
