---
title: Leveraging Python for Real-Time Image processing
speakers:
    - people/elazar-neeman.md
length: 20
language: Hebrew
video_he: https://www.youtube.com/watch?v=4QA0EDjxflY
---


* [a-eye](https://github.com/ElazarNeeman/a-eye)
* [Michal Sela Forum](https://www.michalsela.org.il/)

Discover fascinating world of real-time image and video processing using Python. Learn how we protect women from violent ex-partners by leveraging Python libraries, algorithms, and deep learning models in our Michal Sela Hackathon project.




Whether you're a junior software engineer exploring the realm of image processing or a seasoned developer seeking inspiration, join me on a journey through the fascinating world of real-time image and video processing using Python.

In this talk, I'll share insights from a recent Hackathon project where we tackled the challenge of protecting women from their ex-partner following violent relationship.

I will demonstrate (with code snippets and working examples) how leveraging Python's robust open-source libraries and frameworks, latest algorithms, and deep learning models we have crafted, in a short time, a working solution that pushes the boundaries of traditional image processing techniques.

Furthermore, we'll take you through our Hackathon journey, sharing the highs and lows, the triumphs and setbacks, and the invaluable lessons learned along the way.

We call software and algorithms engineers to join forces and contribute to safe-tech projects like ours. Together let’s make a life-saving impact through innovation.


