---
title: A new way of testing web frameworks
speakers:
    - people/ori-roza.md
length: 20
---

[slides as pdf](/slides/drf-api-action-overview-python-meetup.pdf) - [slides as pptx](/slides/drf-api-action-overview-python-meetup.pptx)


In this talk we will see a new way of testing web frameworks which gives us the ability to treat endpoints as they where a simple functions, with this ability, we can catch simple exceptions and outputs without the web framework layer which gives us simplicity and observability. We will go over a case study of Django web framework. using this [package](https://github.com/Ori-Roza/drf-api-action)

See also [GitHub Pages](https://ori-roza.github.io/).

