---
title: Hacking 'import'
speakers:
    - people/liad-oz.md
length: 30
---

We'll be taking a look at how python 'imports' work behind the scenes and how PYTHONPATH and sys.path affect it. Then learn how to hook into the 'import' logic and show practical (and ridiculous) applications of them.


