---
title: Introduction to Geospatial data and GeoPandas
speakers:
    - people/udi-oron.md
length: 180
language: Hebrew
---



In this workshop we will learn the basic units of geospatial data and how to easily read and process it to create maps using GeoPandas.

Topics:
- Quick overview of maps, projections and coordinate systems.
- Maps: Raster and vector, existing technologies, tile maps.
[Workshop: OpenStreetMap and custom tilemaps]
- Geospatial data types (Points, LineStrings, Polygons) and formats (GeoJSON, WKL, Shapefile)
[Workshop: Query, export and import data online with geojson.io and OverPass Turbo]
- Behind the scenes - Standing on the shoulders of giants: GEOS, shapely (and GDAL, proj)
[Workshop: Creating and manipulating geometries with shapely]
- Crash intro to pandas + Geospatial data analysis with GeoPandas
[Workshop: Reading, displaying, filtering and processing geospatial data with GeoPandas]

Requirements:
- Participants should have at least basic Python background.
- PC+Python+All required libraries installed before the workshop - goepandas, jupyterlab and a few more.

The spoken language used during the workshop is Hebrew. All the written materials are in English.




