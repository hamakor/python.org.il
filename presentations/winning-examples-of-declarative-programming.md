---
title: Winning examples of declarative programming
speakers:
    - people/udi-h-bauman.md
length: 10
---

* [slides](https://speakerdeck.com/dibau_naum_h/declarative-programming-winning-examples)
