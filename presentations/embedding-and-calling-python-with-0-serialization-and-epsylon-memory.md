---
title: Embedding and Calling Python with 0 Serialization and ε Memory
speakers:
    - people/miki-tebeka.md
length: 40
video_he: https://www.youtube.com/watch?v=39jo8WtsOF0
---

* [Slides](https://github.com/tebeka/talks/tree/master/embed-py)
