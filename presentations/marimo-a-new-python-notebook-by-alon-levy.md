---
title: Marimo - a new python notebook
speakers:
    - people/alon-levy.md
length: 10
---

[Marimo](https://marimo.io/) is an open-source reactive notebook for Python — reproducible, git-friendly, executable as a script, and shareable as an app. I'll introduce Marimo from my own usage of it this last 3 months, and show how it can be used for data exploration. I'll show it's unique features resulting from its DAG nature.

[notebook](/slides/alon_pywebil_marimo_demo_2024-09-03-1900.py)

