---
title: "Ctypes on Steroids: Advanced Dynamic Interaction With C"
speakers:
    - people/amir-miron.md
length: 40
---

* [slides](/talks/2016-04-04/CtypesPresentation.zip)
