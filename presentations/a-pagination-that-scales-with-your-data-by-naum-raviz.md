---
title: A pagination that scales with your data
speakers:
    - people/naum-raviz.md
length: 25
---

* What is pagination and why?
* Offset-based Pagination.
* Cursor-based Pagination.
* The ideal solution for pagination

Naum Raviz is a Senior Software engineer at Tenable Attack Path Analysis.

