---
title: Hands-on message broker using Google's pub sub
speakers:
    - people/ohad-perry.md
length: 25
---

* [slides](https://docs.google.com/presentation/d/1yszcUbDsDKAfbFo-XoU0AfZPQ01ySvV7fnb2STCBhDg/edit#slide=id.gc6f73a04f_0_0)
