---
title: GridRoyale, a life simulation for exploring social dynamics
speakers:
    - people/ram-rachum.md
length: 10
video_he: https://www.youtube.com/watch?v=qOirWBZOJxQ
---


* [Slides](https://www.dropbox.com/s/op6w5zo4569kjcx/GridRoyale.pptx?dl=0)
