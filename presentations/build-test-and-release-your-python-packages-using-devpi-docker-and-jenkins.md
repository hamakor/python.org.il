---
title: Build, test and release your python packages using DevPI, Docker and Jenkins
speakers:
    - people/david-melamed.md
length: 30
---

* [slides](/talks/2016-08-01/DevPI.pdf)
