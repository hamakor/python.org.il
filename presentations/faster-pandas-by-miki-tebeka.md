---
title: "Faster Pandas: Make your Pandas code run faster and consume less memory"
speakers:
    - people/miki-tebeka.md
length: 45
---

[Slides](https://www.dropbox.com/s/faq914kj25328jt/slides.pdf?dl=0)
