from bs4 import BeautifulSoup
import os

def setup_module():
    os.system("python generate.py")

def read_file(path):
    with open(path) as fh:
        return fh.read()

def test_index():
    html = read_file("public/index.html")

    assert '<h2 class="title is-2">Planned events</h2>' in html

    soup = BeautifulSoup(html, 'html.parser')
    assert soup.title.string == "Python in Israel - פייתון בישראל"

def test_events():
    html = read_file("public/en/events/index.html")

    assert '<h2 class="title is-2">Planned events</h2>' in html
    assert '<h2 class="title is-2">Past events</h2>' in html

    soup = BeautifulSoup(html, 'html.parser')
    assert soup.title.string == "Python in Israel - פייתון בישראל"


def test_people():
    html = read_file("public/en/people/index.html")
    assert '<h2 class="title is-2">Speakers and organizers</h2>' in html
    assert '<a href="/en/people/asher-sterkin"><img src="/img/asher-sterkin.jpeg" width="128" alt="Asher Sterkin" title="Asher Sterkin"></a>' in html
    assert '<a href="/en/people/asher-sterkin">Asher Sterkin</a>' in html

    soup = BeautifulSoup(html, 'html.parser')
    assert soup.title.string == "Speakers and organizers"

def test_pyweb_2024_08():
    html = read_file("public/en/pyweb-2024-08-at-mobileye.html")
    assert '<a id="implementing-the-composite-design-pattern-in-python-by-asher-sterkin" href="/en/presentations/implementing-the-composite-design-pattern-in-python-by-asher-sterkin">Implementing the Composite Design Pattern in Python</a>' in html
    assert '<a id="accelerating-parquet-file-filters-via-row-groups" href="/en/presentations/accelerating-parquet-file-filters-via-row-groups">Accelerating Parquet file filters via row groups</a>' in html

    assert '<a href="/en/people/asher-sterkin"><img src="/img/asher-sterkin.jpeg" width="256px" alt="Asher Sterkin" title="Asher Sterkin"></a>' in html
    assert '<a href="/en/people/asher-sterkin">Asher Sterkin</a>' in html

    assert '<a href="/en/people/uri-mogilevsky-shay"><img src="/img/uri-mogilevsky-shay.jpeg" width="256px" alt="Uri Mogilevsky Shay" title="Uri Mogilevsky Shay"></a>' in html
    assert '<a href="/en/people/uri-mogilevsky-shay">Uri Mogilevsky Shay</a>' in html

    assert '<a href="/en/people/menachem-kluft">Menachem Kluft</a>' in html

    soup = BeautifulSoup(html, 'html.parser')
    assert soup.title.string == "PyWeb-IL #108 - August 2024"

def test_people_asher_sterkin():
    html = read_file("public/en/people/asher-sterkin.html")

    soup = BeautifulSoup(html, 'html.parser')
    assert soup.title.string == "Asher Sterkin"

    assert '<h1>Asher Sterkin</h1>' in html
    assert '<img style="margin-right: 10px" src="/img/asher-sterkin.jpeg" width="128" alt="Asher Sterkin" title="Asher Sterkin">' in html
    assert '<div><a href="https://www.linkedin.com/in/asher-sterkin-10a1063">LinkedIn</a></div>' in html
    assert '<div><a href="https://www.slideshare.net/AsherSterkin/">SlideShare</a></div>' in html

    assert '<h2>Presentations</h2>' in html
    assert '<li>2024.08.07 <a href="/en/pyweb-2024-08-at-mobileye#implementing-the-composite-design-pattern-in-python-by-asher-sterkin">Implementing the Composite Design Pattern in Python</a> on <a href="/en/pyweb-2024-08-at-mobileye">PyWeb-IL #108 - August 2024</a></li>' in html

def test_people_miki_tebeka():
    html = read_file("public/en/people/miki-tebeka.html")

    soup = BeautifulSoup(html, 'html.parser')
    assert soup.title.string == "Miki Tebeka"

    assert '<div><a href="https://tebeka.github.io/">projects</a></div>' in html

