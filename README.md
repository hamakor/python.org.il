# python.org.il

The site is hosted on GitLab pages. See the pipeline in `.gitlab-ci.yml`.

For historical reasons most of the files are served from the `public/` folder.
Some pages are generated using the `generate.py` program from Markdown files located in the `pages` folder.


## Generate locally

```
pip install -r requirements.txt
python generate.py
```

To view the site locally install [rustatic](https://rustatic.code-maven.com/)

run

```
rustatic --port 5000 --nice --indexfile index.html --path public
```

and then visit http://localhost:5000/


## Testing

There are a few tests checking the results.

```
pip install -r requirements.txt
pytest
```

This will first generate the pages and then check them.



## History

There is an old [repo](https://github.com/Hamakor/python_il_site/) with something that is probably an old version of the web site.
One might want to go digging there for interesting stuff.
