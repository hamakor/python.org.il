---
title: PyWeb Israel - offer you presentation "Call for proposal"
---

## Regular talks (10-40 min)

In order to propose a presentation for [PyWeb-IL](/en/pyweb), please open an issue [in our repo](https://gitlab.com/hamakor/python.org.il/).

* Prefix the title with `[Talk]`.
* Include a detailed description.
* Include the expected length of your presentation in minutes. Presentations usually last between 10-40 minutes including QA.
* Include a few sentences about yourself.
* Include links to your LinkedIn page and GitHub account.
* It would be nice to have a picture of you we can take from one of those account to show on our web site and the promotional banner of the event.
* Include information on which meeting do you prefer to give the presentation. (which month).
* You are also invited to send an email to [Gabor](https://szabgab.com/contact) directly with your phone number so we can be in touch for any last-minute issues.
* Include the language of the presentation. Hebrew or English. The preferred language is Hebrew, though if you feel it would be easier to give the talk in English that is also OK.
* Location: Most often we meet in Tel Aviv, but we also accept invitations in other locations.
* Sometimes we have more talk proposals than slots. We will discuss with you which event can be good for both you and the schedule.

* Speakers usually use their own computers.
* We also like to upload the slides and materials to this site or link to them if the speaker has them published elsewhere. This can be done after the event.

<a class="button is-primary" href="https://gitlab.com/hamakor/python.org.il/-/issues">Propose a presentation</a>

## Lightning talks (up to 5 min)

In addition to the scheduled presentations we also have "lightning talks". These are short, up to 5 min long presentations.
Some lightning talks take less than 1 minute!
Sometimes given without any slides. We don't schedule them up-front. If you show up at our meeting and talk to the
organizer you might get the chance to give your talk. Mostly depending on the available time.

* Give a "taste" of something like your favorite Python library.
* Give a shout-out to someone or an idea: "People please run your tests!".
* Invite people to contribute to your Open Source project.
* ...

## Ideas for presentations

Some random ideas for presentations.

* I wrote this python library that I released to PyPI, let me show you how to use it and how it is written!
* I know how to write a web application in FastAPI, let me teach you the basics!
* I fixed a bug in some open source Python project. Let me show you how!
* We improved the speed of our application 10 times. Let me show how!
* We reduced the memory footprint of our application by 50%. Let me show how!
* We used Python to improve the world by ... Let me show you how we did in and how can you also help!
* Let me show you how to write a game in Python.
* Let me show you how to build a desktop application using the ... GUI framework.
* How to test stuff written in Python?
* How to write easily readable and maintainable code in Python?
* How to interact with ... API in Python?
* ...
