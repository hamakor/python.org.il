---
title: Python communities in Israel
---

# Python communities in Israel

There are many communities in Israel of people using the [Python](http://www.python.org/) programming language.

Below you can find the ones we know about:

* In-person meetings and virtual presentations
    * [PyWeb-IL](/en/pyweb) has monthly in-person meetings with presentations and also virtual events. See the [PyWeb-IL on Meetup](https://www.meetup.com/PyWeb-IL/).
    * [PyData Tel Aviv in Meetup](https://www.meetup.com/pydata-tel-aviv/) has in-person meetings once every few months and an annual conference.
    * [PyCon IL](https://pycon.org.il/) - annual Python conference in Israel.
    * [PyData Tel Aviv 2023](https://pydata.org/telaviv2023/).

* Mailing lists
    * [Pyweb-IL mailing list](http://groups.google.com/group/pyweb-il).

* Facebook
    * [Python Israel](https://www.facebook.com/groups/pythonisraelgroup/)
    * [Python Developers IL](https://www.facebook.com/groups/862322171245805)

* LinkedIn (member numbers on 2025.01.06)
    * [Python Developers Israel](https://www.linkedin.com/groups/8365068/) (746)
    * [Python in Israel](https://www.linkedin.com/groups/3865694/) (516)
    * [Python masters israel](https://www.linkedin.com/groups/13520250/) (283)
    * [Israel Python Leaders ](https://www.linkedin.com/groups/9073989/) (708)

* Telegram
    * [Python in Hebrew](https://t.me/PythonIsrael)

* WhatsApp
    * [Python Developers Israel](https://chat.whatsapp.com/G7rOOKMYIpv1hFJJonwxKw)


## Related groups


* [Baot](https://baot.org/)
* [Baot group on Facebook](https://www.facebook.com/groups/1433362546987936/)
* [Hackeriot](https://www.hackeriot.org/)
* [Appleseeds](https://appleseeds.org.il/)
* [Maakaf - מעק"ף](https://maakaf.netlify.app/)
