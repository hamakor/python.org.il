---
title: Health and Safety policy
---

# Health and Safety Policy

We rely on the instructions and guidelines of the [Ministry of Health](https://www.gov.il/en/departments/ministry_of_health/govil-landing-page), the [Ministry of National Security](https://www.gov.il/en/departments/ministry_of_public_security/govil-landing-page), and the [Home Front Command](https://www.oref.org.il/eng).
