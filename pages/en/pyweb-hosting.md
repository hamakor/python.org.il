---
title: Hosting PyWeb Israel
---

We at [PyWeb-IL](/en/pyweb) we try to have monthly in-person meetings and weekly online presentations. For an in-person PyWeb-IL meeting we need the following:

## Venue requirements

* A space that can host at least 50 people.
* Has a projector and something we can use as a lectern next to the screen where we project.
* Speakers usually use their own computers for the presentations.
* Sponsoring refreshment (see below)
* Preferable in a place that easily accessible with public transportation.
* Our meetings are usually in Tel Aviv, but we are open to host events elsewhere as well.
* We would prefer places that would want to host us regularly every few months, but one-time hosts might be also accepted.


## Refreshment

* Pizza or sandwiches or some more interesting snacks. Some vegetarian, vegan, some gluten-free options. (A total of ~ 2 pieces per attendee.)
* Beer and soft drinks including plain water.
* Optionally fruits.
* Optionally sweets.
* Optionally snacks (Bamba, Bissli, Tapuchips, Snyder's etc.)
* Think also about the people who prefer to eat healthy food.
* Some of us keep Kosher, so taking that in account would be also a good idea.

How much food? We never know how many people will arrive, but our experience shows that it is between 25-40% of the registered people.
You can check the number of registrants right before your place the order for the refreshments.
If you can have some food option that can be packed and consumed the next day by your employees that would be much nicer than throwing away the left-overs.

## Presentations

We can allocate 5-15 minutes at the beginning of the event for the host to give a short introduction of the company.
Alternatively, or even in addition, we welcome employees of the host to give a regular presentation. See [how to submit presentation offer](/en/pyweb-presentations).

Tell us early if you'd like to do either of those so we can make sure to allocate time in the schedule.


* Contact [Gabor Szabo](https://szabgab.com/contact) if you are interested hosting one of our monthly events.
