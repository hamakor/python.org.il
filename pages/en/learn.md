---
title: Learning Python
---


## Videos in Hebrew

* [Shahaf](https://www.youtube.com/@HebrewPython)
* [Python course](https://www.youtube.com/@python_course)
* [Eli Naim](https://www.youtube.com/playlist?list=PLEtZq1oDporhAPIlKhfRCpcxjpXjSeR2z)
* [Python course by Gabor Szabo](https://he.code-maven.com/python)

## Books in Hebrew

* [Python programming](https://data.cyber.org.il/python/python_book.pdf) by Barak Gonen / Cyber Education Center

## Tutorials

* [Python for school](/course/) (a course in Hebrew).
* [FastAPI](https://sweet-conkies-7afd34.netlify.app/public/) (in Hebrew)

## Trainers teaching in Hebrew or English

* [Reuven Lerner](https://lerner.co.il/)
* [Miki Tebeka](https://www.353solutions.com/)
* [Udi Oron](https://www.10x.org.il/)
* [Gabor Szabo](https://szabgab.com/) (The organizer of PyWeb and maintainer of this site)

