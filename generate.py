import datetime
import hashlib
import os
import re
import shutil
from functools import cache
from pathlib import Path

import markdown
import jinja2
import yaml
import glob

EXTRA = ['mingling', 'break', 'lightning-talks']
source_path = "static"
html_path = "public"


def get_file_hash(filepath: Path) -> str:
    """
    Generate MD5 hash of file contents for cache busting.
    Results are cached using functools.cache.
    """
    return hashlib.md5(filepath.read_bytes()).hexdigest()[:8]


@cache
def cache_bust(filepath: str, root=html_path) -> str:
    """
    Jinja2 filter that adds file hash to static asset URLs.
    Hash computation is cached between calls.

    Example:
        {{ '/css/style.css' | cache_bust }}
        -> /css/style.css?v=1a2b3c4d
    """
    h = get_file_hash(Path(root) / filepath.lstrip("/"))
    return f"{filepath}?v={h}"


def main():
    now = datetime.datetime.now()
    today = now.strftime('%Y.%m.%d')

    os.makedirs(html_path, exist_ok=True)

    for folder in os.listdir(html_path):
        if os.path.isdir(os.path.join(html_path, folder)):
            shutil.rmtree(os.path.join(html_path, folder))
        else:
            os.unlink(os.path.join(html_path, folder))

    for folder in os.listdir(source_path):
        if os.path.isdir(os.path.join(source_path, folder)):
            shutil.copytree(os.path.join(source_path, folder), os.path.join(html_path, folder))
        else:
            shutil.copy(os.path.join(source_path, folder), os.path.join(html_path, folder))

    presentations = load_presentations()
    people = load_people()
    update_presentations(presentations, people)
    events = load_events(presentations)

    combine(people, events)
    add_events_to_presentations(presentations, events)
    videos = get_videos(presentations)
    topics = get_topics(presentations)

    future_events = list(filter(lambda event: event["date"] >= today, events))
    past_events=list(filter(lambda event: event["date"] < today, events))

    for folder in ("en", "he"):
        for name in os.listdir(f"pages/{folder}"):
            if not name.endswith(".md"):
                continue
            output = name.replace(".md", ".html")
            render_md(
                source=f"pages/{folder}/{name}",
                template="page.html",
                filename=f"{folder}/{output}",
                today=today,
            )

    dir_path=os.path.join(html_path, "en", "people")
    os.makedirs(dir_path, exist_ok=True)
    render(
        full_path=os.path.join(html_path, "en", "people/index.html"),
        template="people.html",
        today=today,
        people=people,
        lang="en",
    )
    render(
        full_path=os.path.join(html_path, "en", "people/missing.html"),
        template="missing.html",
        today=today,
        people=people,
        lang="en",
    )

    render(
        full_path=os.path.join(html_path, "en", "videos.html"),
        template="videos.html",
        today=today,
        videos=videos,
        lang="en",
    )

    render(
        full_path=os.path.join(html_path, "en", "topics.html"),
        template="topics.html",
        today=today,
        topics=topics,
        lang="en",
    )


    for slug, person in people.items():
        render(
            full_path=os.path.join(html_path, "en", f"people/{slug}.html"),
            template="person.html",
            today=today,
            person=person,
            title=person["name"],
            lang="en",
        )

    render(
        full_path=os.path.join(html_path, "en", "pyweb.html"),
        template="pyweb.html",
        today=today,
        future_events=list(filter(lambda event : event["group"] == "PyWeb" , future_events)),
        past_events=list(filter(lambda event : event["group"] == "PyWeb" , past_events)),
        lang="en",
    )

    render(
        full_path=os.path.join(html_path, "en", "pydata.html"),
        template="pydata.html",
        today=today,
        future_events=list(filter(lambda event : event["group"] == "PyData" , future_events)),
        past_events=list(filter(lambda event : event["group"] == "PyData" , past_events)),
        lang="en",
    )

    render(
        full_path=os.path.join(html_path, "en", "pycon.html"),
        template="pycon.html",
        today=today,
        future_events=list(filter(lambda event : event["group"] == "PyConIL" , future_events)),
        past_events=list(filter(lambda event : event["group"] == "PyConIL" , past_events)),
        lang="en",
    )



    render(
        full_path=os.path.join(html_path, "index.html"),
        template="index.html",
        today=today,
        future_events=future_events,
        lang="en",
    )

    dir_path=os.path.join(html_path, "en", "events")
    os.makedirs(dir_path, exist_ok=True)
    render(
        full_path=os.path.join(html_path, "en", "events", "index.html"),
        template="events.html",
        today=today,
        future_events=future_events,
        past_events=past_events,
        lang="en",
    )


    for event in events:
        if 'slug' in event:
            render(
                full_path=os.path.join(html_path, "en", f"{event['slug']}.html"),
                template="event.html",
                today=today,
                event=event,
                title=event['title'],
                lang="en",
            )

    render(
        full_path=os.path.join(html_path, "en", "presentations", f"index.html"),
        template="presentations.html",
        today=today,
        presentations=presentations,
        lang="en",
    )

    for presentation_slug, presentation in presentations.items():
        if presentation_slug == "index":
            exit("We have a problem here")

        render(
            full_path=os.path.join(html_path, "en", "presentations", f"{presentation_slug}.html"),
            template="presentation.html",
            today=today,
            presentation=presentation,
            title=presentation['title'],
            lang="en",
        )

def get_videos(presentations):
    videos = []
    for slug, data in presentations.items():
        if "video_en" in data:
            videos.append( {
                "language": "en",
                "presentation": data,
            })
        if "video_he" in data:
            videos.append( {
                "language": "he",
                "presentation": data,
            })
    return videos

def get_topics(presentations):
    topics = {}
    for slug, data in presentations.items():
        for topic in data.get('topics', []):
            if topic not in topics:
                topics[topic] = []

            topics[topic].append(data)
    return topics


def combine(people, events):
    for event in events:
        if 'schedule' not in event:
            continue
        for thing in event['schedule']:
            #if thing['presentation'] not in EXTRA:
            #    print(thing)
            #    exit()
            if 'details' in thing and 'speakers' in thing['details']:
                for speaker in thing['details']['speakers']:
                    speaker_slug = speaker['slug']
                    if speaker_slug not in people:
                        exit(f"Unknown speaker: '{speaker_slug}'")
                    people[speaker_slug]['presentations'].append(thing)


def load_events(presentations):
    now = datetime.datetime.now()
    now_str = now.strftime("%Y.%m.%d")

    with open("events.yaml") as fh:
        events = yaml.load(fh, Loader=yaml.Loader)

    for filename in glob.glob("events/*.md"):
        if filename == "events/skeleton.md":
            continue

        header, body = read_md_file(filename)
        slug = os.path.basename(filename[0:-3])
        header["slug"] = slug
        #header["body"] = body
        events.append(header)

    for event in events:
        for field in event:
            if field not in ["date", "title", "register", "group", "slug", "location", "schedule", "banner", "id", "number", "short_summary"]:
                exit(f"Unhandled field '{field}' in event");

        if event["group"] not in ["PyWeb", "PyData", "PyConIL", "PyMaven"]:
            exit(f"Unrecognized group: '{event['group']}'")

    events.sort(key = lambda event: event["date"])

    for event in events:
        if 'slug' in event:
            if 'schedule' not in event:
                exit(f"schedule is missing from event {event['slug']}")
            if 'location' not in event:
                exit(f"location is missing from event {event['slug']}")
            if 'name' not in event['location']:
                exit(f"Field 'name' is missing from location in event '{event['slug']}'")

            if event['location']['name'] in ["Online", "NA"]:
                pass
            else:
                for field in ['url', 'address']:
                    if field not in event['location']:
                        exit(f"Field {field} is missing from location in event {event['slug']}")

            for sch in event['schedule']:
                presentation_slug = sch['presentation']
                if presentation_slug not in EXTRA:
                    # remove the presentations/ from the beginning and .md from the end
                    presentation_slug = presentation_slug[14:-3]
                    sch['details'] = presentations[presentation_slug]
                sch['presentation_slug'] = presentation_slug

            event['future'] = event['date'] > now_str


    return events

def load_people():
    people = {}
    names = {}

    for file in glob.glob("people/*.md"):
        if file == "people/skeleton.md":
            continue

        try:
            person = read_person(file)
        except Exception as err:
            exit(f"Exception {err} while reading {file}")

        for field, value in person.items():
            if field not in [
                    "name",
                    "github", "gitlab", "linkedin", "homepage",
                    "img",
                    "twitter",
                    "SlideShare", "Slides", "SlideDeck", "SpeakerDeck",
                    "company", "projects", "blog",
                    "body",
                    "slug",
                ]:
                exit(f"Unrecognized field in the '{file}' file: '{field}'")
            #if value.startswith("http://"):
            #    exit(f"URL with http '{value}'")

        # check that we don't have the exact same name twice
        name = person['name'].lower()
        assert name not in names, f"duplicate name: '{name}'"
        names[name] = True

        if 'img' in person and person['img'] is not None:
            img_path = os.path.join(html_path, "img", person['img'])
            if not os.path.exists(img_path):
                exit(f"image '{img_path}' does not exist")

        github = person.get('github', '')
        if github is not None:
            if not re.search("^[a-zA-Z0-9-]*$", github):
                exit(f"Invalid github field: {github} in {file}")

        person['presentations'] = []

        slug = person['slug']
        assert slug not in people
        people[slug] = person

    return people


def render_md(source, template, filename=None, **args):
    with open(source) as fh:
        text = fh.read()

    match = re.search("^---\n(.*)\n---\n(.*)$", text, re.DOTALL)
    if not match:
        raise Exception(f"parsing error in {source}")
    (header, content) = match.groups()
    headers = {}
    for row in header.split("\n"):
        (key, value) = row.split(":")
        headers[key] = value

    content = md2html(content)

    lang = "en"
    if filename.startswith("en/"):
        lang = "en"
    if filename.startswith("he/"):
        lang = "he"

    full_path = os.path.join(html_path, filename)

    render(full_path, template, **args, **headers, content=content, lang=lang)

def md2html(content):
    content = markdown.markdown(content, extensions=['tables'])
    content = re.sub('<h1>', '<h1 class="title is-1">', content)
    content = re.sub('<h2>', '<h2 class="title is-2">', content)
    content = re.sub('<h3>', '<h3 class="title is-3">', content)
    return content

def render(full_path, template, **args):
    root = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.join(root, "templates")
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(templates_dir), autoescape=True)
    env.filters['cache_bust'] = cache_bust

    html_template = env.get_template(template)
    html = html_template.render(**args)

    dir_path = os.path.dirname(full_path)
    os.makedirs(dir_path, exist_ok=True)

    with open(full_path, "w") as fh:
        fh.write(html)

def read_md_file(filename):
    with open(filename) as fh:
        first = fh.readline()
        assert first == "---\n", f"first line is not --- in '{filename}'"
        header = ""
        for line in fh:
            if line == "---\n":
                break
            header += line
        body = fh.read()

    header = yaml.load(header, Loader=yaml.Loader)

    return header, body

def read_person(filename):
    header, body = read_md_file(filename)
    slug = os.path.basename(filename[0:-3])
    header['slug'] = slug
    header['body'] = md2html(body)
    return header


def load_presentations():
    with open("topics.csv") as fh:
        topics = list(filter(lambda line: line, map(lambda line: line.strip(), fh.readlines())))

    presentations = {}
    for filename in glob.glob("presentations/*.md"):
        if filename == "presentations/skeleton.md":
            continue

        slug = os.path.basename(filename[0:-3])
        header, body = read_md_file(filename)
        for field in header:
            if field not in ["title", "speakers", "length", "video_en", "video_he", "topics", "language"]:
                exit(f"Unhandled field '{field}' in {filename}")

        if "language" in header:
            if header["language"] not in ["English", "Hebrew"]:
                exit(f"Invald language field '{header["language"]}' in {filename}")
        else:
            header["language"] = "Hebrew" # default

        for topic in header.get("topics", []):
            if topic not in topics:
                exit(f"Unknown topic '{topic}' in '{filename}' {topics}")

        header['body'] = md2html(body)
        presentations[slug] = header
    return presentations

def update_presentations(presentations, people):
    for presentation_slug, presentation in presentations.items():
        details = []
        for speaker_filename in presentation['speakers']:
            if not os.path.exists(speaker_filename):
                exit(f"Speaker file '{speaker_filename}' does not exist. Mentioned in presentation '{presentation_slug}'")
            speaker_slug = os.path.basename(speaker_filename[0:-3])
            if speaker_slug not in people:
                exit(f"Speaker '{speaker_slug}' of file '{speaker_filename}' does not exist.")
            details.append(people[speaker_slug])
        presentation['speakers'] = details

def add_events_to_presentations(presentations, events):
    for event in events:
        if 'slug' not in event:
            continue
        event_slug = event['slug']
        #print(event_slug)
        for pres in event['schedule']:
            presentation_slug = pres['presentation_slug']
            if presentation_slug in EXTRA:
                continue

            if presentation_slug not in presentations:
                exit(f"Can't find presentation '{presentation_slug}'")
            #print(event)
            presentations[presentation_slug]['event'] = {
                    'date': event['date'],
                    'title': event['title'],
                    'slug': event['slug'],
                }


main()
