Python-C Bindings

1. Categorization of P-X interfaces

Python is written in C, and provides special API for connecting with it. 
This is used in several ways, each for it's own purposes.
We will focus on the last - Python bindings for libraries...

1.1 Embedding Python in X

 Some applications embed python as a scripting language (like emacs uses elisp).
 Typicly app would fire up a python interpreter (through API call) when it's run, 
 or when it first need to access python.

 e.g.: Gimp's python-fu

 purpose: 
    provide fast and easy interface for configuration and user exrensions.

1.2 Reimplement Python

 Some languages/programming envs provide their own VM. 
 e.g. Jython (Java), IronPython (.net), pirate (parrot)

 purpose: If you implement python on these VM's, you automaticly get access to all libs 
   written for that platform. Some VM's were designed to be multilingual, so we'd want
   to have python on them too.

 problem: Incompatabilities with official python. Does not work if you wish to combine
   with extensions written in C.

1.3 Extending Python

 You can write Python modules in pure C (or any other language that can be compiled to
 generate a shared object/DLL containing C-like functions and structs).

  e.g.: cPickle, other std modules, numpy (Numeric/numarray)

  purpose:
    Usually for more efficient implementation.

  Note1: There's pyrex - a python-like compiled language designed specificly for writing 
    python extensions.

  Note2: psyco project (JIT type-tuned compiler) is alternative approach to time 
  optimization.

1.4 Providing Python Bindings
 
 As in prev category, python sees module. But module is just a thin "skin" and main
 actions are performed in other code (not related to python).

 Subtypes:

 1.4.1 Connecting to other App
  Typically connect to some other App/VM (often firing it up from python), and use 
  it's C-API.
  
  e.g: Python.Net, JPype, PyMat

  purpose: 
    Directly use third party functionality available on the the other app without having
    to rewrite it for python (or reimplement python).
    As opposed to communications via sockets or pipes, this way you can directly pass
    memory pointers back and forth (e.g. in PyMat, duplication and copying large numeric 
    arrays).

 1.4.2 Python Bindings to a library

  Will see examples. Module handles parameter passing and conversion. Sometimes also 
  provides a more "pythonic" frontend (e.g. classes if C lib).

  e.g.: PyGame (SDL bindings), pygtk

  purpose:
    * Reuse existing non-python code which might be well optimised and/or widely tested.
    * Many libraries were specificly designed to be low level core used by other 
    languages, as well as apps.
    * For lib-developers: with new existing automatic binding tools, you can focus on 
    developing the low level library, and have python bindings with very little effort.

2 Methods for python bindings

You can use python's raw C-API as explained in 2.1.
However we will see that it's cumbersome, error-prone and hassle-full.
Many tools exist, using different and various approaches to solve this.

2.1 Raw C API

 Advantages: been there forever, provided with core python (no requirements),
 low-level control (lower than pure python).
 Disadvantages - see below.

 2.1.1
  http://www.python.org/doc/2.4.2/ext/ext.html
  http://www.python.org/doc/2.4.2/api/api.html

  Explanations, pointers to references...

  Code Examples (excerpts from PyGame?)

 2.1.2 Problems and gotchas
  * Lot of boilerplate code
  * Takes a lot of code to implement new types (lot of struct slots to take care of).
  * For C++ code: have to manually reflect the existing class structure
  * Hard to keep track of the INCREF's

2.2 Harnessing the power of C++ compiler

 * CXX - More C++'ish API. Uses object constructions to handle refcounting.
  http://cxx.sourceforge.net/PyCXX.html

 * boost.python - A more complete solution, provides templates and other constructs, 
  to ease up describing the module you wish to write and minimize boilerplate.
  http://www.boost.org/libs/python/doc/index.html

2.3 Code Generators

 * SWIG - Old, well known, not python-specific.
  http://www.swig.org/
 * SIP - (c++ ish)
  http://www.riverbankcomputing.co.uk/sip/

 These ease up the work alot, but it appears that (especially with C++) there's still
 a lot of definitions you have to do, to describe your types and interfaces.
 Must "understand" the (C++) language to automate some of this.

 2.3.1 
  * pyplusplus - uses gcc-xml to parse your C++ code, then uses this "understanding"
    to automaticly generate boost.python code. Provides ways to tune and control this
    code automation.

   http://pygccxml.sourceforge.net/pyplusplus/pyplusplus.html

  * robin - Uses doxygen to parse C/C++, but almost completely hands-free. 

   http://robin.python-hosting.com

   As opposed to other tools, robin does not create a python module for the wrapped lib.
   Instead it connects to the non-python-specific lib at runtime.
   This is almost like the "ctypes" approach described below, however it still generates
   some C code (just one static struct which is either compiled into your lib, or as a 
   seperate library - e.g. if you don't have the sources).

2.4 ctypes

  Different approach:

  * Uses the existing binary library (opens it with dlopen).
  * Provide basic tools for defining C types and conversions.

  Now you can write the whole binding in *pure python*.

  Explanations, pointers to refs
  Show examples.

  http://starship.python.net/crew/theller/ctypes/

  Problems:
   * Does not provide automatic code generation.
   * Does not understand C++ ABI. 
    C++ ABI is still non standard and very compiler specific,
    Still, writing C++ binding for common compilers seems a good future direction (and 
    it can even be done in pure python ;-) )
