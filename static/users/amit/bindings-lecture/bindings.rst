=================
Python-C Bindings
=================

.. contents::

1. Intro
========

In OSDC Israel 2006, three Python binding tools are presented (two newborn
Py-C++ projects and a relatively new Py-C one).

This short intro is intended to give a broader overview of the topic,
mention some of the available tools and the major differences between them.

We are going to mention many tools in a short time and maybe even flash some
code samples. To put some order into things (and make it less tedious), I'll
present a rough categorization of Python interfaces with other languages 
(sec 2), then zoom in on one category - Python-C/C++ bindings.

1.1 About
---------
Python is general purpose, and has both low-level OS-related libraries
and a very wide scope standard library (not to mention the add-on packages).

However, interfacing with other languages is an important need. There are
many different cases and reasons where this becomes relevat. To name a few:

 1. Quick way to get Python access to existing functionalities (avoiding
    rewrite, making use of existing stable and well tested code).

 2. Use specific powers of other language - mainly C/C++/Fortran for
    efficient execution (but see notes_).

 3. Easier multi-platform support, by using existing libraries designed
    for being multi-platform and cross-programming-language (e.g. GTK+).

 4. Provide Python as a powerful extension mechanism for some applications.

2. Categorization of P-X interfaces
===================================

Python is written in C, and provides special API for connecting with it. 
This is used in several ways, each for it's own purposes.
We will present a few, then focus on the last - Python bindings for libraries...

2.1 Embedding Python in X
-------------------------

 Some applications embed python as a scripting language (like emacs uses elisp).
 Typicly app would fire up a python interpreter (through API call) when it's run, 
 or when it first need to access python.

 e.g.: Gimp's python-fu.

 purpose: 
    provide fast and easy interface for configuration and user exrensions.

2.2 Reimplementing Python
-------------------------

 Some languages/programming envs provide their own VM.
 This VM can run a whole new implementation of Python, which is not necessarily
 slower than C-Python.

 e.g. Jython (Java), IronPython (.net), Pirate (parrot)

 purpose: 
   If you implement python on these VM's, you automaticly get access to all libs 
   written for that platform. Some VM's were designed to be multilingual, so we'd want
   to have python on them too.

 problem: 
   Incompatabilities with official python. Does not work if you wish to combine
   with extensions written in C.

2.3 Extending Python
--------------------

 You can write Python modules in pure C (or any other language that can be compiled
 to generate a shared object/DLL containing C-like functions and structs).

 e.g.: cPickle, other std modules, numpy (Numeric/numarray)

 purpose:
   Usually for more efficient implementation.

.. _notes:

  **Note1:** There's pyrex - a python-like compiled language designed specificly for 
  writing python extensions.

  **Note2:** psyco project (JIT type-tuned compiler) is alternative approach to time 
  optimization.

2.4 Providing Python Bindings
-----------------------------
 
 As in prev category, python sees a module. But this module is just a thin 
 "skin", and the main actions are performed in other code (not related to 
 python).

 "Subcategories":

2.4.1 Connecting to another Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
  Typically connect to some other App/VM (sometimes firing it up from python), and use 
  it's C-API.
  
  e.g: Python.Net, JPype, PyMat

  purpose: 
    Directly use third party functionality available on the the other app without having
    to rewrite it for python (or reimplement python).
    As opposed to communications via sockets or pipes, this way you can directly pass
    memory pointers back and forth (e.g. in PyMat, duplication and copying large numeric 
    arrays).
    Note that while PyMat has a "thin" layer of integration, JPype and Python.Net support
    seemless integration - looks like native Python.

2.4.2 Python Bindings to a library
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Will see examples. The module handles parameter passing and conversion. Sometimes also 
  provides a more "pythonic" frontend (e.g. classes if interfacing a C lib).

  e.g.: PyGame (SDL bindings), pygtk

  purpose:
    * Reuse existing non-python code which might be well optimised and/or widely tested.
    * Many libraries were specificly designed to be low level core used by other 
      languages, as well as apps.
    * For lib-developers: with new existing automatic binding tools, you can focus on 
      developing the low level library, and have python bindings with very little effort.

3. Methods for python bindings
==============================

You can use python's raw C-API as explained in 3.1.
However we will see that it's cumbersome, error-prone and hassle-full.
Many tools exist, using different and various approaches to solve this.

3.1 Raw C API
-------------

 Advantages: been there forever, provided with core python (no requirements),
 low-level control (lower than pure python).
 Disadvantages - see below.

3.1.1 Descriptions
~~~~~~~~~~~~~~~~~~

  If have time, try to briefly follow tutorial in:

  http://www.python.org/doc/2.4.2/ext/ext.html

  Occasionally refer to:

  http://www.python.org/doc/2.4.2/api/api.html

  Code Examples (currently extracted from py-docs):
   `simple C module <api_example.html>`_ - show:
     * The init method
     * The ``PyMethodDef``
     * spam_system: ``PyArg_ParseTuple`` and ``Py_BuildValue``
     * my_set_callback: Note ``Py_INCREF`` and friends.

   `module defining new types <api_type_example.html>`_ - show:
     * ``NoddyObject`` and ``NoddyType`` - list of slots in type object.
     * The calls in ``initnoddy``.

3.1.2 Problems and gotchas
~~~~~~~~~~~~~~~~~~~~~~~~~~

  * Lot of boilerplate code

  * Takes a lot of code to implement new types (lot of struct slots to take care of).

  * For wrapping C++ code: have to manually reflect the existing class structure.

  * Hard to keep track of the INCREF's

3.2 Harnessing the power of C++ compiler
----------------------------------------

 * CXX - More C++'ish API. Uses object constructions to handle refcounting.

   http://cxx.sourceforge.net/PyCXX.html

   (some code samples there)

 * boost.python - A more complete solution, provides templates and other constructs, 
   to ease up describing the module you wish to write and minimize boilerplate.

   http://www.boost.org/libs/python/doc/index.html

3.3 Code Generators
-------------------

 * SWIG - Old, well known, not python-specific. Might be considered the "mainstream
   wrapping tool".

   http://www.swig.org/

   code examples in http://www.swig.org/tutorial.html

 * SIP - C++-ish, developed for PyQT.

   http://www.riverbankcomputing.co.uk/sip/

   example: http://www.riverbankcomputing.com/Docs/sip4/sipref.html#a-simple-c-example

 These ease up the work alot, but it appears that (especially with C++) there's still
 a lot of definitions you have to do, to describe your types and interfaces.
 Must "understand" the (C++) language to automate some of this.

3.3.1 Syntax Aware Generators
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  * pyplusplus - uses gcc-xml to parse your C++ code, then uses this "understanding"
    to automaticly generate boost.python code. Provides ways to tune and control this
    code automation.

    http://pygccxml.sourceforge.net/pyplusplus/pyplusplus.html

  * robin - Uses doxygen to parse C/C++, but almost completely hands-free. 

    http://robin.python-hosting.com

    As opposed to other tools, robin does not create a python module for the wrapped lib.
    Instead it connects to the non-python-specific lib at runtime.
    This is almost like the "ctypes" approach described below, however it still generates
    some C code (just one static struct which is either compiled into your lib, or as a 
    seperate library - e.g. if you don't have the sources).

3.4 ctypes
----------

  Different approach:

  * Uses the existing binary library (opens it with dlopen).
  * Provides basic tools for defining C types and conversions.
  * There's a new automatic code generator, relying on gcc-xml.

  Now you can write the whole binding in *pure python*.

  follow tutorial in:

  http://starship.python.net/crew/theller/ctypes/tutorial.html

  Show examples.

  Problems:

  * Does not understand C++ ABI. 

    C++ ABI is still non standard and very compiler specific.
    Still, writing C++ binding for common compilers seems a good future direction (and 
    it can even be done in pure python ;-) )
