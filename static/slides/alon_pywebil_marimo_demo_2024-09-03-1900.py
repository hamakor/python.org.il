import marimo

__generated_with = "0.8.4"
app = marimo.App(
    width="medium",
    layout_file="layouts/alon_pywebil_marimo_demo_2024-09-03-1900.slides.json",
)


@app.cell
def __(left_about_marimo, mo, right_about_marimo, rtl):
    mo.vstack([
        mo.md("# Marimo"),
        mo.hstack([mo.md(left_about_marimo), rtl(mo.md(right_about_marimo))]),
    ])
    return


@app.cell
def __(left_about_marimo, mo, right_about_marimo, rtl, toggle_rtl):
    _opt_rtl = rtl if toggle_rtl.value else lambda x: x
    mo.vstack([
        mo.md("# Marimo 2"),
        mo.hstack([mo.md(left_about_marimo), _opt_rtl(mo.md(right_about_marimo))]),
        mo.hstack([toggle_rtl]),
    ])
    return


@app.cell
def __(mo):
    toggle_rtl = mo.ui.checkbox(label='RTL', value=True)
    return toggle_rtl,


@app.cell
def __():
    right_about_marimo = r"""
    * ?????????? ?????? ?????????? ??????????????????
    * ????????????
    * ?????? ???????????? ????????????
    """
    return right_about_marimo,


@app.cell
def __():
    left_about_marimo = r"""
    * Main distinguishing feature: All variables are tracked between cells.
    * Next: easy plotting, gui
    * Can be used as an app and a script easily (```python notebook.py```, ```marimo run notebook.py```)
    """
    return left_about_marimo,


@app.cell
def __():
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        # Exposition

        - marimo is a reimagining of the python notebook (jupyter / jupyterlab) set out to solve:
            - Reproducibility
            - Versioning
            - Reactivity
        """
    )
    return


@app.cell
def __():
    # Reproducability
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        - assign a variable
        - use it in a second cell
        - delete first cell
        - see second cell fail on <span style="color: purple">NameError</span>
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        # Plot

        - make two static vectors, import plot and plot it, then change static to linspace+sin, then add n, then dropdown, customize; sidebar
        - wave controllable by number of points (slider), sin/cos (checkbox), frequency (dropdown), and show histogram using selection (plot.value)
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        """
        # Some real world notebook

        Show (assuming I can copy the database) the summary db
        """
    )
    return


@app.cell
def __(mo):
    mo.md("""# Dictionary""")
    return


@app.cell
def __():
    from english_dictionary.scripts import read_pickle as ed_read_pickle
    return ed_read_pickle,


@app.cell
def __(ed_read_pickle):
    p = ed_read_pickle.get_dict()
    words = dict(p)
    return p, words


@app.cell
def __(p, pl):
    words_df = pl.DataFrame([{'word': x[0], 'meaning': x[1]} for x in p.items()])
    return words_df,


@app.cell
def __(mo):
    word = mo.ui.text(label='word')
    return word,


@app.cell
def __(defaultdict):
    letters = defaultdict
    return letters,


@app.cell
def __(mo, word, words):
    mo.vstack([
        word,
        word.value in words
    ])
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        # Reactivity
        ## Where I show a signal generator
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        - Demos:
            - reproducibility: no cell remains stale by tracking dependencies between variables
                - trivial demonstration
            - git friendliness: source is straight python. Show the source for this talk.
                - do not stop to talk about the additions.
            - complex UI - my example? too much overhead explaining about concrete and ultrasound ; perhaps wrap in a simpler story.
            - Tables - load a table, plot it via UI.
            - mandelbrot?
            - serial terminal? interesting to anyone?
            - perhaps get ideas from the crowd?
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        # Feature check list

        - reload cells on cell change (optionally lazy)
        - expunge deleted variables
        - reload on external module change (optionally lazy, optionally off)
        - install packages semi-automatically
        - variables pane
        - named cells
        - slide mode, grid mode
        - plots integration
        - reactivity: ui is just a variable that changes (checkbox.value)
        - keyboard shortcuts editable
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        """
        # Not going over

        - AI integration
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        Things I don't like about marimo / miss compared to jupyter/ipython:

        1. No "?"
        2. No "??"
        3. No easy way to jump to source outside of the notebook
        4. RTL
        5. No collaboration
        6. No debugger
        7. Cannot reopen same tab on another computer (but run can)
        8. Other languages

        Things I really like

        1. Fast pace of development
        2. Fast reaction to open tickets in github
        3. Runtime cell indicator

        Meh

        1. But you can break it! it's python.
        """
    )
    return


@app.cell
def __(mo):
    mo.md(r"""# Misc""")
    return


@app.cell
def __(mo):
    mo.md(r"""## Can we embed Html inside Html? Yes!""")
    return


@app.cell
def html_in_html(mo):
    _a = mo.Html('<b>hello</b>')
    mo.Html(f'<div dir=rtl>{_a}</div>')
    return


@app.cell
def rtl(mo):
    rtl = lambda x: mo.Html(f'<div dir=rtl>{x}</div>')
    return rtl,


@app.cell
def __(which):
    marimo_full = which('marimo')
    return marimo_full,


@app.cell
def __(Path, os):
    def which(x):
        for p in os.environ['PATH'].split(':'):
            p = Path(p)
            if (p / x).exists():
                return str(p / x)
        return None
    return which,


@app.cell
def __(PIPE, Popen, marimo_full, sys):
    tutorial = Popen([sys.executable, marimo_full], stdout=PIPE)
    return tutorial,


@app.cell
def __(tutorial):
    tutorial.stdout.read().decode()
    return


@app.cell
def __():
    return


@app.cell
def talk_todo(mo):
    mo.md(
        r"""
        # Talk TODO

        1. Ideas: show how I make a "which" utility, in marimo. Of course it will be the 5th time and not the 1st time. Get suggestions from the crowd.
        1. Record myself giving a presentation with what I have right now, make sure it fits 10 minutes. Needs to include
            4. overview.
            5. some demos
            6. pain points
            7. references
        """
    )
    return


@app.cell
def __(mo):
    mo.md(
        r"""
        # Bugs to report

        - enhancement: RTL support for markdown
        - change font size in slide mode
        - disable slides by cell annotation (hide slides, common feature)
        - goto stack (symbol stack) and back button (there is already a goto symbol)
        - (feat-bug) variable pane does not show inclusion in unparsable cells
        - cannot assign Ctrl-Shift-5, saved as Ctrl-Shift-% - seems trivial to fix
        - !! match statement breaks marimo reactivity
        """
    )
    return


@app.cell
def __():
    import sys
    import os
    from subprocess import Popen, PIPE
    from pathlib import Path
    import marimo as mo
    from collections import defaultdict
    return PIPE, Path, Popen, defaultdict, mo, os, sys


@app.cell
def __():
    return


if __name__ == "__main__":
    app.run()
